//
//  iClinicTests.swift
//  iClinicTests
//
//  Created by Onur Küçük on 24.04.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import XCTest
import UIKit
import Firebase

class iClinicTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        FIRApp.configure()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
