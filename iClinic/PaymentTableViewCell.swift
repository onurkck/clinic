//
//  PaymentTableViewCell.swift
//  iClinic
//
//  Created by Onur Küçük on 26.12.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class PaymentTableViewCell: UITableViewCell {
    @IBOutlet weak var paymentDateLabel:UILabel!
    @IBOutlet weak var paidAmountLabel:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWithDBFinancialHist(financialHist:FinancialHistory) {
        self.paymentDateLabel.text = DateConversion.sharedInstance.convertDateToDBFormat(date: financialHist.paymentDate)
        self.paidAmountLabel.text = "\(financialHist.paidAmount) TL"
    }

}
