//
//  EditingPatientTableViewCell.swift
//  iClinic
//
//  Created by Onur Küçük on 3.03.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import UIKit

class EditingPatientTableViewCell: UITableViewCell {
    @IBOutlet weak var textView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
