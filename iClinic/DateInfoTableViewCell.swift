//
//  DateInfoTableViewCell.swift
//  BlindIDStats
//
//  Created by Onur Küçük on 23.10.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class DateInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var dateTypeLabel: UILabel!
    @IBOutlet weak var selectedDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
