//
//  Dentist.swift
//  iClinic
//
//  Created by Onur Küçük on 29.10.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit
import FirebaseAuth

class Dentist: NSObject {

    var dentistID:String?
    var dentistName:String!
    var dentistSurname:String!
    var dentistPhone:Int!
    var dentistEmail:String!
    var dentistPassword:String!
    
    static func initFIRUser(object:FIRUser) -> Dentist {
        let dentist = Dentist()
        dentist.dentistID = object.uid
        dentist.dentistEmail = object.email!
        return dentist
    }
}
