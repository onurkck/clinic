//
//  LoginViewController.swift
//  iClinic
//
//  Created by Onur Küçük on 8.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var clinicName: UITextField!
    @IBOutlet weak var clinicPassword: UITextField!
    var backendService:Services?

    override func viewDidLoad() {
        super.viewDidLoad()
        backendService = ServicesImplementation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func loginWithClinicNameAndPassword(_ sender: UIButton) {
        UtilFunctions.sharedInstanste.showLoader(viewController: self)
        let clinic = Clinic()
        clinic.clinicName = clinicName.text
        clinic.clinicPassword = clinicPassword.text
//        backendService?.loginWithClinic(clinic:clinic, completionFunction: { (success, error) in
//            
//            UtilFunctions.sharedInstanste.stopActivityIndicator(viewController: self)
//            
//            if success {
//                //Go to the main storyboard
//            }
//            else {
//                let alertController = UtilFunctions.sharedInstanste.showAlertController(withTitle: "Hata", andMessage: (error?.localizedDescription)!)
//                self.present(alertController, animated: true, completion: nil)
//            }
//        })
    }
    
    @IBAction func requestAPassword(_ sender: UIButton) {
        let alertController = UtilFunctions.sharedInstanste.showAlertController(withTitle: "Üzgünüz", andMessage: "Yeni klinik kayıtları şu anda alınmamaktadır. Daha sonra tekrar deneyiniz.")
        self.present(alertController, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
