//
//  CreatingPatientTableViewCell.swift
//  iClinic
//
//  Created by Onur Küçük on 30.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit
//protocol PatientCellDelegate {
//    func textEnteringFinished(enteredValue:String?)
//}
class CreatingPatientTableViewCell: UITableViewCell {
    @IBOutlet weak var textField:UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textField.layer.cornerRadius = 3.0
        self.textField.layer.borderWidth = 1.5
        self.textField.layer.borderColor = UIColor(colorLiteralRed: 188/255, green: 109/255, blue: 181/255, alpha: 1).cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWith(placeHolderText:String) {
        self.textField.placeholder = placeHolderText
    }
    
    func configureCellForEditing(placeHolder:String, value:String) {
        self.textField.placeholder = placeHolder
        self.textField.text = value
    }
}
