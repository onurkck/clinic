//
//  Clinic.swift
//  iClinic
//
//  Created by Onur Küçük on 29.10.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit
import Parse
class Clinic: NSObject {

//    var clinicID:String?
    var clinicName:String!
    var clinicPassword:String!
    
    static func initParseObject(object:PFObject) -> Clinic {
        let clinic = Clinic()
        clinic.clinicName = object.objectId
        clinic.clinicName = object.object(forKey: "clinicName") as! String
        return clinic
    }
}
