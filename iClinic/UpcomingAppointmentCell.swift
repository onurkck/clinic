//
//  PatientTableViewCell.swift
//  iClinic
//
//  Created by Onur Küçük on 11.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

protocol UpcomingAppointmentDelegate {
    func appointmentNoteWasEnteredWith(appointmentWithPatient:Appointment)
}

class UpcomingAppointmentCell: UITableViewCell, UITextViewDelegate {
    @IBOutlet weak var patientNameSurname: UILabel!
    @IBOutlet weak var appointmentNotes: UITextView!
    @IBOutlet weak var patientFinancialStatus:UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var callButton: UIButton!
    var patientPhone:Int!
    var delegate:UpcomingAppointmentDelegate?
    var appointment:Appointment!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureAppointmentCell(){
        let patient = appointment.patient
        self.patientNameSurname.text = "\(patient!.patientName) \(patient!.patientSurname)"
        self.timeLabel.text = "\(appointment.startTime!) - \(appointment.endTime!)"
        self.appointmentNotes.text = "\(appointment.appointmentNotes)"
        patientPhone = patient!.patientPhone

    }
    
    @IBAction func callPatient(){
        let phoneString = "tel://0\(patientPhone!)"
        print("Patient Phone: \(phoneString)")
        UIApplication.shared.openURL(URL(string: phoneString)!)
    }
    
    //MARK: TextView Delegate
    
   
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" { // Recognizes enter key in keyboard
            textView.resignFirstResponder()
            if textView.text != appointment.appointmentNotes {
                appointment.appointmentNotes = textView.text!
                delegate?.appointmentNoteWasEnteredWith(appointmentWithPatient: appointment)
            }
            return false
        }
        return true
    }

}
