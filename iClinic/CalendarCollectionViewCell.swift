//
//  CalendarCollectionViewCell.swift
//  iClinic
//
//  Created by Onur Küçük on 18.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

protocol CalendarCellDelegate {
    func dateSelectedFromTextView(date:String)
}

class CalendarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var summaryTextView: UITextView!
    var delegate:CalendarCellDelegate?
    var dateString = ""
    
    func configureCellWithPatient(date:String, appointments:[Appointment]) -> CalendarCollectionViewCell{
        self.dateLabel.text = date.firstFourChars
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dateSelectedViaTextView))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        summaryTextView.addGestureRecognizer(tapGesture)
        var summaryText = ""
        dateString = date
        if appointments.count > 0 {
            for appointment in UtilFunctions.sharedInstance.sortPatientsWithTime(arrayForSorting: appointments) {
                let startEndTime = "\(appointment.startTime!)-\(appointment.endTime!)"
                summaryText += "\(startEndTime) \(appointment.patient!.patientName) \(appointment.patient!.patientSurname)\n"
                
            }
            self.summaryTextView.text = summaryText
        }
        else {
            self.summaryTextView.text = "Randevu Yok"
        }
        return self
    }
    
    
    func dateSelectedViaTextView(){
        self.delegate?.dateSelectedFromTextView(date: dateString)
    }
}
