//
//  DatePickerTableViewCell.swift
//  BlindIDStats
//
//  Created by Onur Küçük on 22.10.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class DatePickerTableViewCell: UITableViewCell {
    @IBOutlet weak var datePicker: UIDatePicker!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func dateChanged(_ sender: UIDatePicker) {
        
        print(datePicker.date)
        
//        var strDate = dateFormatter.stringFromDate(myDatePicker.date)
//        self.selectedDate.text = strDate
    }

}
