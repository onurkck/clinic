//
//  PatientsForDateVC.swift
//  iClinic
//
//  Created by Onur Küçük on 30.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class PatientsForDateVC: BaseVC, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, AppointmentDelegate, UpcomingAppointmentDelegate, NoteChangedAppointmentDetailDelegate {
    @IBOutlet weak var tableView:UITableView!
    var appointments = [Appointment]()
    var selectedAppointment:Appointment!
    var selectedPatientFinancialStatus:Finance!
    var selectedDate = Date()
    var cellTextView:UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.createAppointmentForSelectedDate))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - DB Related Functions
    func getAppointments(){
        dbService?.getAppointmentsFromDB(datesArrayInString: [DateConversion.sharedInstance.convertDateToDBFormat(date: selectedDate)], completionFunction: { (returnedAppointments:[Appointment]?, error:Error?) in
            if error == nil, let returneds = returnedAppointments {
                self.appointments = UtilFunctions.sharedInstance.sortPatientsWithTime(arrayForSorting: returneds)
                self.tableView.reloadData()
            }
        })
    }
    
    func deleteAppointment(appointment:Appointment){
        dbService?.deleteAppointment(appointment: appointment, completionFunction: { (dbSuccess:Bool, error:Error?) in
            if dbSuccess == true {
                print("\nAPPOINTMENT WAS DELETED FROM DB SUCCESSFULLY.")
            }
            else {
                print("\nAPPOINTMENT COULDN'T DELETED FROM DB")
            }
        })
        fireService?.deleteAnAppointment(appointment: appointment, completionFunction: { (success:Bool, error:Error?) in
            if success == true {
                print("\nAPPOINTMENT WAS DELETED FROM FIREBASE SUCCESSFULLY.")
            }
            else {
                print("\nAPPOINTMENT COULDN'T DELETED FROM FIREBASE.")
            }
        })
    }
    
    func updateAppointmentNote(selectedAppointment:Appointment){
        
        dbService?.updateAppointmentWith(appointment: selectedAppointment, completionFunction: { (wasUpdated:Bool, error:Error?) in
            if error == nil && wasUpdated {
                print("\nAPPOINTMENT NOTE UPDATED SUCCESSFULLY ON DB")

            }
            else {
                print("\nAPPOINTMENT NOTE COULDN'T UPDATE ON DB")
            }
        })
        
        fireService?.updateAppointmentNote(appointment: selectedAppointment, completionFunction: { (success:Bool, error:Error?) in
            if error == nil && success {
                print("\nAPPOINTMENT NOTE UPDATED ON THE FIREBASE SUCCESSFULLY")
            }
            else {
                print("\nAPPOINTMENT COULDN'T UPDATE ON THE FIREBASE")
            }
        })
    }
    //MARK: Table View Related
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if appointments.count > 0 {
            return appointments.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if appointments.count == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "NoAppointment", for: indexPath)
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientCell", for: indexPath) as! UpcomingAppointmentCell
        let appointment = appointments[indexPath.row]
        cell.delegate = self
        cell.appointment = appointment
        cell.configureAppointmentCell()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedAppointment = self.appointments[indexPath.row]
        dbService?.getPatientFinanceStatus(patient: self.selectedAppointment.patient!, completionFunction: { (returnedFinance:Finance?, error:Error?) in
            if error == nil {
                self.selectedPatientFinancialStatus = returnedFinance!
                self.performSegue(withIdentifier: "goAppointmentDetails", sender: self)
            }
        })
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if appointments.count > 0 {
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if appointments.count > 0 {
            if editingStyle == UITableViewCellEditingStyle.delete {
                let willDeletedAppointment = appointments[indexPath.row]
                appointments.remove(at: indexPath.row)
                tableView.reloadData()
                deleteAppointment(appointment: willDeletedAppointment)
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if appointments.count == 0 {
            return 44.0
        }
        return 224.0
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    //MARK: Delegates
    func appointmentCreated() {
        self.getAppointments()
    }
    
    func createAppointmentForSelectedDate() {
        self.performSegue(withIdentifier: "createAppointment", sender: self)
    }
    
    func appointmentNoteWasEnteredWith(appointmentWithPatient: Appointment) {
        //Changes appointment text
        if let foundIndex = appointments.index(where: {$0.appointmentID == appointmentWithPatient.appointmentID}) {
            appointments[foundIndex] = appointmentWithPatient
        }
        self.updateAppointmentNote(selectedAppointment: appointmentWithPatient)
    }
    
    func noteChanged() {
        self.getAppointments()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "createAppointment" {
            let destVC = segue.destination as! CreateAppointmentVC
            var dateString = DateConversion.sharedInstance.convertDateToDBFormat(date: selectedDate)
            dateString.append(" 09:00")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            destVC.startDate = dateFormatter.date(from: dateString)!
            destVC.delegate = self
        }
        else if segue.identifier == "goAppointmentDetails"{
            let destVC = segue.destination as! AppointmentDetailVC
            destVC.appointmentWithPatient = self.selectedAppointment
            destVC.patientObject = selectedAppointment.patient!
            destVC.patientFinancialStatus = self.selectedPatientFinancialStatus
            destVC.doesItComeFromPatientDetail = false
            destVC.delegate = self
        }
    }
    
    
}
