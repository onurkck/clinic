//
//  PatientDetailTableViewCell.swift
//  iClinic
//
//  Created by Onur Küçük on 15.12.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class PatientDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var totalAmountTextField: UITextField!
    @IBOutlet weak var paidAmountTextField: UITextField!
    
    @IBOutlet weak var balanceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCellWith(patient:Patient, financialStatus:Finance) {
        self.totalAmountTextField.text = "\(financialStatus.totalCost)"
        self.paidAmountTextField.text = ""
        self.balanceLabel.text = "Kalan: \(financialStatus.balance)"
    }
}
