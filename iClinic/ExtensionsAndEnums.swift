//
//  ExtensionsAndEnums.swift
//  iClinic
//
//  Created by Onur Küçük on 9.05.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import UIKit

extension Date {
    var toString:String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        return dateFormatter.string(from: self as Date)
    }
}

extension String {
    var firstTwoCharacters:String{
        return self.substring(to: self.index(startIndex, offsetBy: 2))
    }
    
    var firstFourChars:String{
        return self.substring(to: self.index(startIndex, offsetBy: 5))
    }
    
    var firstCharacter:String{
        return self.substring(to: self.index(startIndex, offsetBy: 1))
    }
    
    var convertToDate:Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        return dateFormatter.date(from: self)! as Date
    }
}
extension Bool {
    var getIntegerValue:Int{
        if self == true {
            return 1
        }
        else {return 0}
    }
}
extension UITextField {
    var getIntegerValue:Int {
        if self.text == "" || self.text == nil {
            return 0
        }
        else {
            return (Int)(self.text!)!
        }
    }
}

extension Int {
    var toBool:Bool {
        return self == 1
    }
}

enum UIUserInterfaceIdiom : Int {
    case unspecified
    
    case phone // iPhone and iPod UI
    case pad // iPad style UI
}

