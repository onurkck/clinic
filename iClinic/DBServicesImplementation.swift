//
//  DBServicesImplementation.swift
//  iClinic
//
//  Created by Onur Küçük on 8.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit
import FMDB

class DBServicesImplementation: DBServices {
    
    
    //MARK: - Basic CRUD Operations
    
    func createTablesWith(dbName:DatabaseName, columnsAndTypes:[Column], completionFunction:@escaping BoolReturnFunction){
        
        let fileManager = FileManager.default
        let databasePath = self.getDatabasePath(withDatabaseName: dbName)
        print("DB PATH : \(databasePath)")
        
        var createTableStatement = "CREATE TABLE IF NOT EXISTS \(dbName.rawValue)("
        
        if !fileManager.fileExists(atPath: databasePath) {
            let createdDB = FMDatabase(path: databasePath)
            
            if createdDB == nil {
                print("Error : \(String(describing: createdDB?.lastErrorMessage()))")
                completionFunction(false,createdDB?.lastError())
            }
            
            if (createdDB?.open())!{
                for column in columnsAndTypes {
                    createTableStatement += "\(column.name) \(column.type),"
                }
                //                createTableStatement.remove(at: createTableStatement.index(before: createTableStatement.endIndex))
                createTableStatement += "UNIQUE(ID))"
                print(createTableStatement)
                
                if !(createdDB?.executeStatements(createTableStatement))!{
                    print("Error: \(String(describing: createdDB?.lastErrorMessage()))")
                    completionFunction(false,createdDB?.lastError())
                }
                createdDB?.close()
                completionFunction(true,nil)
            }
            else {
                print("\(dbName.rawValue)DB couldn't opened and the error is \(String(describing: createdDB?.lastError().localizedDescription))")
            }
        }
        
    }
    
    func insertDatas(dbName: DatabaseName, datas:[String], completionFunction:@escaping BoolReturnFunction){
        var columns = [Column]()
        switch dbName {
        case DatabaseName.AppointmentDatabase:
            columns = Statics.appointmentColumnsAndTypes
        case DatabaseName.PatientDatabase:
            columns = Statics.patientColumnsAndTypes
        case DatabaseName.FinanceDatabase:
            columns = Statics.financeColumnsAndTypes
        case DatabaseName.FinancialHistDatabase:
            columns = Statics.financialHistoryColumnsAndTypes
        case DatabaseName.Treatment:
            columns = Statics.treatmentColumnsAndTypes
        }
        
        let dbPath = self.getDatabasePath(withDatabaseName: dbName)
        let db = FMDatabase(path: dbPath)
        
        print("Database Path: \(dbPath)")
        
        DispatchQueue.main.async {
            
            if (db?.open())! == true {
                
                var insertStatement = "INSERT OR IGNORE INTO \(dbName.rawValue)("
                for column in columns {
                    insertStatement += "\(column.name),"
                }
                insertStatement.remove(at: insertStatement.index(before: insertStatement.endIndex))
                insertStatement += ") VALUES ("
                
                for i in 0...datas.count - 1 {
                    if columns[i].type == "TEXT" {
                        insertStatement += "'\(datas[i])',"
                    }
                    else {
                        insertStatement += "\(datas[i]),"
                    }
                }
                insertStatement.remove(at: insertStatement.index(before: insertStatement.endIndex))
                insertStatement += ")"
                print(insertStatement)
                
                let result = db?.executeStatements(insertStatement)
                
                if result! {
                    print("---- INSERTING DATAS TO \(dbName.rawValue) WAS SUCCESSFUL")
                    completionFunction(true,nil)
                }
                else {
                    print("----- WHILE INSERTING DATAS TO \(dbName.rawValue) OCCURED AN ERROR --> \(String(describing: db?.lastErrorMessage()))")
                    completionFunction(false,db?.lastError())
                }
                db?.close()
            }
        }
    }
    
    func updateData(dbName:DatabaseName, objectID:String, object:AnyObject, completionFunction:@escaping BoolReturnFunction){
        var objectDatas = [String]()
        switch dbName {
        case DatabaseName.AppointmentDatabase:
            objectDatas = (object as! Appointment).configureAppointmentForDB()
        case DatabaseName.PatientDatabase:
            objectDatas = (object as! Patient).configurePatientForDB()
        case DatabaseName.FinanceDatabase:
            objectDatas = (object as! Finance).configureFinanceForDB()
        case DatabaseName.FinancialHistDatabase:
            objectDatas = (object as! FinancialHistory).configureFinancialHistForDB()
        case DatabaseName.Treatment:
            print("configure treatments")
        }
        
        let dbPath = self.getDatabasePath(withDatabaseName: dbName)
        let queue = FMDatabaseQueue(path: dbPath)
        
        queue?.inDatabase({ (database) in
            let deleteStatement = "DELETE FROM \(dbName.rawValue) WHERE ID='\(objectID)'"
            
            let result = database?.executeStatements(deleteStatement)
            
            if result == true {
                self.insertDatas(dbName: dbName, datas: objectDatas, completionFunction: { (success:Bool, error:Error?) in
                    
                    if error == nil {
                        print("After deleting object, inserting status is : \(success)")
                        completionFunction(true, nil)
                    }
                    else {
                        print("After deleting object, an error occured ---> \(String(describing: error?.localizedDescription))")
                    }
                })
                
            }
            else {
                print("----- WHILE UPDATING DATAS TO \(dbName.rawValue) OCCURED AN ERROR --> \(String(describing: database?.lastErrorMessage()))")
                completionFunction(false,database?.lastError())
            }
        })
        
    }
    
    func clearDatabase(dbName:DatabaseName, completionFunction: @escaping BoolReturnFunction) {
        let dbPath = self.getDatabasePath(withDatabaseName: dbName)
        let db = FMDatabase(path: dbPath)
        
        if db?.open() == true {
            let deleteStatement = "DELETE FROM \(dbName.rawValue)"
            db?.executeUpdate(deleteStatement, withArgumentsIn: nil)
            db?.close()
        }
    }
    
    //MARK: - Appointment Related Function
    
    func getAppointmentHistoryOfPatient(patient:Patient, completionFunction:@escaping AppointmentArrayReturnFunction){
        var appointmentsOfPatient = [Appointment]()
        let dbPath = self.getDatabasePath(withDatabaseName: .AppointmentDatabase)
        let db = FMDatabase(path: dbPath)
        
        if db?.open() == true {
            let selectStatement = "SELECT*FROM \(DatabaseName.AppointmentDatabase.rawValue) WHERE PATIENT_ID='\(patient.patientID)' ORDER BY DATE DESC"
            do {
                let result = try (db?.executeQuery(selectStatement, values: nil))
                while result?.next() == true {
                    appointmentsOfPatient.append(Appointment.initAppointmentResultSet(resultSet: result!))
                }
                completionFunction(appointmentsOfPatient, nil)
            }
            catch  {
                completionFunction(nil,db?.lastError())
            }
        }
        db?.close()
    }
    
    func getAppointmentsFromDB(datesArrayInString:[String], completionFunction:@escaping AppointmentArrayReturnFunction){
        var appointmentDBArray = [Appointment]()
        let dbPath = self.getDatabasePath(withDatabaseName: DatabaseName.AppointmentDatabase)
        let db = FMDatabase(path: dbPath)
        
        if db?.open() == true {
            for dateString in datesArrayInString {
                let selectStatement = "SELECT*FROM \(DatabaseName.AppointmentDatabase.rawValue) WHERE DATE='\(dateString)'"
                
                do {
                    let result = try (db?.executeQuery(selectStatement, values: nil))
                    while result?.next() == true{
                        appointmentDBArray.append(Appointment.initAppointmentResultSet(resultSet: result!))
                    }
                }
                catch{
                    completionFunction(nil,error)
                }
            }
            completionFunction(appointmentDBArray,nil)
        }
        db?.close()
        
    }
    
    func getAppointmentNote(appointment:Appointment, completionFunction:@escaping AppointmentArrayReturnFunction){
        let db = FMDatabase(path: getDatabasePath(withDatabaseName: .AppointmentDatabase))
        if db?.open() == true {
        }
    }
    
    func updateAppointmentWith(appointment:Appointment, completionFunction:@escaping BoolReturnFunction){
        let dbPath = self.getDatabasePath(withDatabaseName: DatabaseName.AppointmentDatabase)
        let db = FMDatabase(path: dbPath)
        
        if db?.open() == true {
            let updateQuery = "UPDATE \(DatabaseName.AppointmentDatabase.rawValue) SET APPOINTMENT_NOTES='\(appointment.appointmentNotes)' WHERE ID='\(appointment.appointmentID)'"
            let result = db?.executeUpdate(updateQuery, withArgumentsIn: nil)
            if db?.hadError() == true {
                completionFunction(false, db?.lastError())
            }
            else {
                completionFunction(result!, nil)
            }
        }
        db?.close()
    }
    
    func getPatientsFromDB(appointments:[Appointment], completionFunction:@escaping PatientArrayReturnFunction) {
        var patientDBArray = [Patient]()
        let dbPath = self.getDatabasePath(withDatabaseName: DatabaseName.PatientDatabase)
        let db = FMDatabase(path: dbPath)
        
        if db?.open() == true {
            for appointment in appointments {
                let selectStatement = "SELECT*FROM \(DatabaseName.PatientDatabase.rawValue) WHERE ID='\(appointment.patientID)'"
                do {
                    let result = try (db?.executeQuery(selectStatement, values: nil))
                    while result?.next() == true{
                        patientDBArray.append(Patient.initPatientResultSet(resultSet: result!))
                    }
                    completionFunction(patientDBArray, nil)
                }
                catch{
                    completionFunction(nil,error)
                }
            }
        }
        db?.close()
    }
    
    func deleteAppointment(appointment:Appointment, completionFunction:@escaping BoolReturnFunction) {
        let db = FMDatabase(path: getDatabasePath(withDatabaseName: .AppointmentDatabase))
        
        if db?.open() == true {
            let deleteStatement = "DELETE FROM \(DatabaseName.AppointmentDatabase.rawValue) WHERE ID='\(appointment.appointmentID)'"
            
            let result = db?.executeUpdate(deleteStatement, withArgumentsIn: nil)
            if result == true {
                completionFunction(true, nil)
            }
            else {
                completionFunction(false, db?.lastError())
            }
        }
        else {
            completionFunction(false, db?.lastError())
        }
        db?.close()
    }
    
    //MARK: Patient Related Functions
    
    func getAllPatientsFromDB(completionFunction:@escaping PatientArrayReturnFunction){
        var patientDBArray = [Patient]()
        let dbPath = self.getDatabasePath(withDatabaseName: DatabaseName.PatientDatabase)
        let db = FMDatabase(path: dbPath)
        
        if db?.open() == true {
            let selectStatement = "SELECT*FROM \(DatabaseName.PatientDatabase.rawValue)"
            do {
                let result = try (db?.executeQuery(selectStatement, values: nil))
                while result?.next() == true{
                    patientDBArray.append(Patient.initPatientResultSet(resultSet: result!))
                }
                completionFunction(UtilFunctions.sharedInstance.sortDBPatientArray(arrayForSorting: patientDBArray), nil)
            }
            catch{
                completionFunction(nil,error)
            }
        }
        db?.close()
    }
    
    func deletePatient(patient:Patient, completionFunction:@escaping BoolReturnFunction){
        self.deletePatientAppointmentsWithPatient(patient: patient)
        self.deletePatientFinanceObj(patient: patient)
        let db = FMDatabase(path: getDatabasePath(withDatabaseName: .PatientDatabase))
        if db?.open() == true {
            let deleteStatement = "DELETE FROM \(DatabaseName.PatientDatabase.rawValue) WHERE ID='\(patient.patientID)'"
            let result = db?.executeUpdate(deleteStatement, withArgumentsIn: nil)
            if result == true {
                completionFunction(true,nil)
            }
            else {
                completionFunction(false, db?.lastError())
            }
        }
        db?.close()
    }
    
    
    func getPatientWith(patientID:String, completionFunction:@escaping PatientReturnFunction){
        let db = FMDatabase(path: getDatabasePath(withDatabaseName: DatabaseName.PatientDatabase))
        
        if db?.open() == true {
            let selectStatement = "SELECT*FROM \(DatabaseName.PatientDatabase.rawValue) WHERE ID='\(patientID)'"
            do {
                let result = try (db?.executeQuery(selectStatement, values: nil))
                while result?.next() == true {
                    completionFunction(Patient.initPatientResultSet(resultSet: result!), nil)
                }
            }
            catch{
                print(db!.lastError().localizedDescription)
                completionFunction(nil,db?.lastError())
            }
        }
        db?.close()
    }
    
    //MARK: Treatment
    
    //    func getTreatmentTypes(completionFunction:@escaping DBTreatmentArrayReturnFunction){
    //        var treatmentDBArray = [DBTreatment]()
    //        let dbPath = self.getDatabasePath(withDatabaseName: DatabaseName.Treatment)
    //        let db = FMDatabase(path: dbPath)
    //
    //        if db?.open() == true {
    //            let selectStatement = "SELECT*FROM \(DatabaseName.Treatment.rawValue)"
    //            do {
    //                let result = try (db?.executeQuery(selectStatement, values: nil))
    //                while result?.next() == true{
    //                    treatmentDBArray.append(DBTreatment.initTreatmentResultSet(resultSet: result!))
    //                }
    //                completionFunction(treatmentDBArray, nil)
    //            }
    //            catch{
    //                completionFunction(nil,error)
    //            }
    //        }
    //        db?.close()
    //    }
    
    //MARK: Finance
    
    func getPatientFinanceStatus(patient:Patient, completionFunction:@escaping FinanceReturnFunction){
        let dbPath = self.getDatabasePath(withDatabaseName: DatabaseName.FinanceDatabase)
        let db = FMDatabase(path: dbPath)
        let selectStatement = "SELECT*FROM \(DatabaseName.FinanceDatabase.rawValue) WHERE ID='\(patient.patientFinancialStatID)'"
        if db?.open() == true {
            do {
                let resultSet = try (db?.executeQuery(selectStatement, values: nil))
                while resultSet?.next() == true{
                    completionFunction(Finance.initFinanceResultSet(resultSet: resultSet!), nil)
                }
                
            }
            catch{
                completionFunction(nil, error)
            }
        }
        db?.close()
    }
    
    func updateFinanceStatusOfPatient(patient:Patient, financeObj:Finance, completionFunction:@escaping BoolReturnFunction){
        let dbPath = self.getDatabasePath(withDatabaseName: DatabaseName.FinanceDatabase)
        let db = FMDatabase(path: dbPath)
        
        if db?.open() == true {
            
            let updateQuery = "UPDATE \(DatabaseName.FinanceDatabase.rawValue) SET TOTAL_AMOUNT=\(financeObj.totalCost), PAID_AMOUNT=\(financeObj.paidAmount), BALANCE=\(financeObj.balance), IS_THERE_ANY_PAYMENT=\(financeObj.isThereAnyPayment.getIntegerValue), FINANCE_NOTES='\(financeObj.financeNotes)', LAST_PAYMENT_DATE='\(financeObj.lastPaymentDate.toString)' WHERE ID='\(patient.patientFinancialStatID)'"
            let result = db?.executeUpdate(updateQuery, withArgumentsIn: nil)
            
            if db?.hadError() == true {
                completionFunction(false, db?.lastError())
            }
            completionFunction(result!, nil)
        }
        db?.close()
    }
    
    func getPatientFinancialHistory(patient:Patient, completionFunction:@escaping FinancialHistoryObjectArrayReturnFunction){
        var financialHistArray = [FinancialHistory]()
        let db = FMDatabase(path: getDatabasePath(withDatabaseName: .FinancialHistDatabase))
        if db?.open() == true {
            let selectQuery = "SELECT * FROM \(DatabaseName.FinancialHistDatabase.rawValue) WHERE PATIENT_ID='\(patient.patientID)'"
            do {
                let resultSet = try (db?.executeQuery(selectQuery, values: nil))
                while resultSet?.next() == true {
                    financialHistArray.append(FinancialHistory.initFinancialHistResultSet(resultSet: resultSet!))
                }
                completionFunction(financialHistArray, nil)
            }
            catch{
                completionFunction(nil, db?.lastError())
            }
        }
        db?.close()
    }
    
    //MARK: - Helper Functions
    func getDatabasePath(withDatabaseName:DatabaseName) -> String{
        let dirPaths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        let docsDir = dirPaths[0]
        return ("\(docsDir + withDatabaseName.rawValue).db")
    }
    
    func deletePatientAppointmentsWithPatient(patient:Patient) {
        let db = FMDatabase(path: getDatabasePath(withDatabaseName: .AppointmentDatabase))
        
        if db?.open() == true {
            let deleteStatement = "DELETE FROM \(DatabaseName.AppointmentDatabase.rawValue) WHERE PATIENT_ID='\(patient.patientID)'"
            let result =  db?.executeUpdate(deleteStatement, withArgumentsIn: nil)
            if result == true {
                print("\nPATIENT APPOINTMENTS DELETED FROM DB SUCCESSFULLY")
                
            }
            else {
                print("\nPATIENT APPOINTMENTS COULDN'T DELETE FROM DB")
            }
        }
        db?.close()
    }
    
    func deletePatientFinanceObj(patient:Patient) {
        let db = FMDatabase(path: getDatabasePath(withDatabaseName: .FinanceDatabase))
        
        if db?.open() == true {
            let deleteStatement = "DELETE FROM \(DatabaseName.FinanceDatabase.rawValue) WHERE ID='\(patient.patientFinancialStatID)'"
            let result =  db?.executeUpdate(deleteStatement, withArgumentsIn: nil)
            if result == true {
                print("\nPATIENT APPOINTMENTS DELETED FROM DB SUCCESSFULLY")
                
            }
            else {
                print("\nPATIENT APPOINTMENTS COULDN'T DELETE FROM DB")
            }
        }
        db?.close()
    }
}
