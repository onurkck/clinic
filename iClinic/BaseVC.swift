//
//  BaseVC.swift
//  iClinic
//
//  Created by Onur Küçük on 11.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {
    
    var fireService:FirebaseService?
    var dbService:DBServices?
    var activityIndicator:UIActivityIndicatorView!
    var loaderView:UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        fireService = FirebaseImplementation()
        dbService = DBServicesImplementation()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: DB Related Functions
    func updateOrInsertNewDatas(databaseName:DatabaseName,objectID:String, object:AnyObject) {
        self.dbService?.updateData(dbName: databaseName, objectID: objectID, object: object, completionFunction: { (status:Bool, error:Error?) in
            if error == nil {
                print("\nUPDATING \(databaseName.rawValue) DB OBJECT STATUS IS : \(status)")
            }
            else {
                print("\nAN ERROR OCCURED WHILE UPDATING \(databaseName.rawValue) DB OBJECT")
            }
        })
    }
    
    //MARK: - Util Functions
    func showAlertController(withTitle:String, andMessage:String){
        let alertController = UIAlertController(title: withTitle, message: andMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showActivityIndicator(){
        self.view.isUserInteractionEnabled = false
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        loaderView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        loaderView.alpha = 0.6
        loaderView.center = self.view.center
        loaderView.backgroundColor = UIColor.black
        loaderView.layer.cornerRadius = 10
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        loaderView.addSubview(activityIndicator)
        self.view.addSubview(loaderView)
    }
    
    func stopActivityIndicator(){
        self.view.isUserInteractionEnabled = true
        loaderView.removeFromSuperview()
        activityIndicator.stopAnimating()
    }
}
