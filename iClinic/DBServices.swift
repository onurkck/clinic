//
//  DBServices.swift
//  iClinic
//
//  Created by Onur Küçük on 8.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import Foundation

enum DatabaseName : String {

    case PatientDatabase = "PATIENT"
    case AppointmentDatabase = "APPOINTMENT"
    case FinanceDatabase = "FINANCE"
    case FinancialHistDatabase = "FINANCIAL_HISTORY"
    case Treatment = "TREATMENT"
}

protocol DBServices {

    //MARK: - Basic CRUD Operations
    
    func createTablesWith(dbName:DatabaseName, columnsAndTypes:[Column], completionFunction:@escaping BoolReturnFunction)
    
    func insertDatas(dbName: DatabaseName, datas:[String], completionFunction:@escaping BoolReturnFunction)
    
    func updateData(dbName:DatabaseName, objectID:String, object:AnyObject, completionFunction:@escaping BoolReturnFunction)
    
    func clearDatabase(dbName:DatabaseName, completionFunction:@escaping BoolReturnFunction)
    
    //MARK: Appointment Related Functions
    
    func getAppointmentHistoryOfPatient(patient:Patient, completionFunction:@escaping AppointmentArrayReturnFunction)
    
    func getAppointmentsFromDB(datesArrayInString:[String], completionFunction:@escaping AppointmentArrayReturnFunction)
    
    func getAppointmentNote(appointment:Appointment, completionFunction:@escaping AppointmentArrayReturnFunction)
    
    func updateAppointmentWith(appointment:Appointment, completionFunction:@escaping BoolReturnFunction)
    
    func deleteAppointment(appointment:Appointment, completionFunction:@escaping BoolReturnFunction)
    
    //MARK: Patient Related Functions
    
    func getAllPatientsFromDB(completionFunction:@escaping PatientArrayReturnFunction)
    
    func deletePatient(patient:Patient, completionFunction:@escaping BoolReturnFunction)
    
    func getPatientWith(patientID:String, completionFunction:@escaping PatientReturnFunction)

    //MARK: Treatment
    
    
    //MARK: Finance
    
    func getPatientFinanceStatus(patient:Patient, completionFunction:@escaping FinanceReturnFunction)
    
    func updateFinanceStatusOfPatient(patient:Patient, financeObj:Finance, completionFunction:@escaping BoolReturnFunction)
    
    func getPatientFinancialHistory(patient:Patient, completionFunction:@escaping FinancialHistoryObjectArrayReturnFunction)

}
