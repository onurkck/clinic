//
//  AppointmentHistoryTableViewCell.swift
//  iClinic
//
//  Created by Onur Küçük on 13.12.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit
protocol AppointmentHistoryDelegate {
    func appointmentNoteWasEnteredWith(appointment:Appointment)
}

class AppointmentHistoryTableViewCell: UITableViewCell, UITextViewDelegate {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var notesTextView: UITextView!
    
    var delegate:AppointmentHistoryDelegate?
    var Appointment:Appointment!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWithAppointment() {
        dateLabel.text = "\(Appointment.date!)"
        timeLabel.text = "\(Appointment.startTime!)-\(Appointment.endTime!)"
        notesTextView.text = Appointment.appointmentNotes
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" { // Recognizes enter key in keyboard
            textView.resignFirstResponder()
            if notesTextView.text != Appointment.appointmentNotes {
                Appointment.appointmentNotes = notesTextView.text!
                delegate?.appointmentNoteWasEnteredWith(appointment: Appointment)
            }
            return false
        }
        return true
    }

}
