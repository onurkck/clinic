//
//  FirebaseImplementation.swift
//  iClinic
//
//  Created by Onur Küçük on 18.04.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import Foundation
import Firebase

class FirebaseImplementation:FirebaseService {
    
    var fireRootRef = FIRDatabase.database().reference()
    var appointmentsRef = FIRDatabase.database().reference().child("appointments")
    var patientsRef = FIRDatabase.database().reference().child("patients")
    var radiographyRef = FIRDatabase.database().reference().child("images").child("radiographies")
    var photosRef = FIRDatabase.database().reference().child("images").child("photos")
    let dbService:DBServices = DBServicesImplementation()
    
    //MARK: Patient Related Functions
    
    func createPatient(patient:Patient, completionFunction:@escaping BoolReturnFunction)
    {
        let newPatientRef = self.patientsRef.child(currentDentist.dentistID!).childByAutoId()
        let financeID = FIRDatabase.database().reference().childByAutoId().key
        patient.patientID = newPatientRef.key
        patient.patientFinancialStatID = financeID
        self.createFinanceObjectForPatient(patient: patient)
        
        newPatientRef.setValue(["patientID":patient.patientID, "patientName":patient.patientName, "patientSurname":patient.patientSurname, "patientPhone":patient.patientPhone, "patientEmail":patient.patientEmail, "patientFinanceID":patient.patientFinancialStatID, "patientHealedToothIDs":patient.healedTooth, "patientUnhealedToothIDs":patient.unhealedTooth, "patientRemovedToothIDs":patient.removedTooth], withCompletionBlock: {(error:Error?, ref:FIRDatabaseReference) -> Void in
            if error == nil {
                self.dbService.insertDatas(dbName: .PatientDatabase, datas: patient.configurePatientForDB()) { (success:Bool, error:Error?) in
                    if error == nil {
                        print("Patient was created successfully on DB")
                    }
                    else {
                        print("ERROR: Patient couldn't be created on DB\n\(error!.localizedDescription)")
                    }
                }
                completionFunction(true, nil)
            }
            else {
                print(" - FIRLOG --> create patient error \(error!.localizedDescription)")
                
                completionFunction(false, error)
            }
        })
    }
    
    func getPatientWith(id:String, completionFunction:@escaping PatientReturnFunction){
        self.patientsRef.child(currentDentist.dentistID!).child(id).observeSingleEvent(of: .value) { (snapshot:FIRDataSnapshot) in
            if let value = snapshot.value as? NSDictionary {
                completionFunction(Patient.initPatientWith(dictionary: value), nil)
            }
            else {
                print("There is no patient with this id")
                completionFunction(nil,nil)
            }
        }
    }
    
    func updatePatient(patient:Patient, completionFunction:@escaping BoolReturnFunction){
        self.patientsRef.child(currentDentist.dentistID!).child(patient.patientID).updateChildValues(patient.convertToDict(), withCompletionBlock: {(error:Error?, ref:FIRDatabaseReference) -> Void in
            if error == nil {
                completionFunction(true, nil)
            }
            else {
                print(" - FIRLOG --> update patient error \(error!.localizedDescription)")
                
                completionFunction(false, error)
            }
        })
    }
    
    func deletePatient(patient:Patient, completionFunction:@escaping BoolReturnFunction){
        self.patientsRef.child(currentDentist.dentistID!).child(patient.patientID).removeValue(completionBlock: {(error:Error?, ref:FIRDatabaseReference) -> Void in
            if error == nil {
                completionFunction(true, nil)
            }
            else {
                print(" - FIRLOG --> delete patient error \(error!.localizedDescription)")
                completionFunction(false, nil)
            }
        })
    }
    
    func getDentistPatients(dentist:Dentist, completionFunction:@escaping PatientArrayReturnFunction){
        var patientArray = [Patient]()
        self.fireRootRef.child("patients").child(currentDentist.dentistID!).observeSingleEvent(of: .value) { (snapshot:FIRDataSnapshot) in
            if let value = snapshot.value as? NSDictionary {
                let patients = value.allValues as! [NSDictionary]
                for patient in patients {
                    let initiatedPatient = Patient.initPatientWith(dictionary: patient)
                    patientArray.append(initiatedPatient)
                }
            }
            else{
                print("The dentist doesn't have any patient")
            }
            completionFunction(patientArray,nil)
        }
    }
    
    func getPatientPhotos(patient:Patient, isRadiography:Bool, completionFunction:@escaping PatientPhotoArrayReturnFunction){
        var imageRootRef:FIRDatabaseReference!
        var patientPhotos = [PatientPhoto]()
        if isRadiography {
            imageRootRef = self.radiographyRef
        } else {
            imageRootRef = self.photosRef
        }
        imageRootRef.child(patient.patientID).observeSingleEvent(of: .value) { (snapshot:FIRDataSnapshot) in
            if let childNodes = snapshot.children.allObjects as? [FIRDataSnapshot] {
                for childNode in childNodes {
                    let image = childNode.value as! NSDictionary
                    let tempPhoto = PatientPhoto.initPatientPhotoWith(dictionary: image)
                    patientPhotos.append(tempPhoto)
                }
                completionFunction(patientPhotos,nil)
            }
            else {
                completionFunction(nil, errSecBadReq as? Error)
            }
        }
    }
    
    func uploadPatientPhoto(patient:Patient, image:UIImage, isRadiography:Bool, completionFunction:@escaping BoolReturnFunction){
        var imageRootRef = ""
        if isRadiography {
            imageRootRef = "radiographies"
        } else {
            imageRootRef = "photos"
        }
        let uid = UUID.init()
        let imageData = UIImageJPEGRepresentation(image, 0.6)!
        let imageRef = FIRStorage.storage().reference().child(imageRootRef).child(currentDentist.dentistID!).child(patient.patientID).child("\(uid).jpg")
        let uploadTask = imageRef.put(imageData, metadata: nil) { (returnMetadata:FIRStorageMetadata?, error:Error?) in
            guard let metadata = returnMetadata else {
                completionFunction(false,error!)
                return
            }
            self.saveImageURLOfPatient(patient: patient, url: metadata.downloadURL()!, isRadiography: isRadiography)
        }
        uploadTask.resume()
        
        uploadTask.observe(.success) { (snapshot:FIRStorageTaskSnapshot) in
            uploadTask.removeAllObservers()
            completionFunction(true,nil)
        }
    }
    
    func download(patientPhoto:PatientPhoto, completionFunction:@escaping ImageReturnFunction){
        let storage = FIRStorage.storage()
        let httpsRef = storage.reference(forURL: patientPhoto.downloadURLString)
        httpsRef.data(withMaxSize: 2 * 1024 * 1024) { (returnedData:Data?, err:Error?) in
            if err == nil, let data = returnedData {
                completionFunction(UIImage(data:data),nil)
            }
            else {
                completionFunction(nil, err)
            }
        }
    }
    
    //MARK: Appointment Related Functions
    
    func createAppointment(appointment:Appointment, completionFunction:@escaping BoolReturnFunction){
        let appointmentReference = self.appointmentsRef.child(currentDentist.dentistID!).child(DateConversion.sharedInstance.convertDateToFireNodeFormat(date: appointment.startDateTime)).childByAutoId()
        appointment.appointmentID = appointmentReference.key
        LocalNotification.shared.setNotificationFor(appointment: appointment)
        dbService.insertDatas(dbName: .AppointmentDatabase, datas: appointment.configureAppointmentForDB()) { (success:Bool, error:Error?) in
            if error == nil && success {
                print("Appointment was created successfully on DB")
            }
            else {
                print("Appointment couldn't be created on DB\n\(error!.localizedDescription)")
            }
        }
        appointmentReference.setValue(["appointmentID":appointment.appointmentID,"appointmentStartDate":appointment.startDateTime.toString, "appointmentEndDate":appointment.endDateTime.toString, "appointmentNotes":appointment.appointmentNotes, "patient":appointment.patient!.patientID], withCompletionBlock: {(error:Error?, ref:FIRDatabaseReference) -> Void in
            if error == nil {
                completionFunction(true,nil)
            }
            else {
                completionFunction(false,error!)
            }
        })
    }
    
    func updateAppointmentNote(appointment:Appointment, completionFunction:@escaping BoolReturnFunction){
        let fireDateNode = DateConversion.sharedInstance.convertDateToFireNodeFormat(date: appointment.startDateTime)
        let appointmentReference = self.appointmentsRef.child(currentDentist.dentistID!).child(fireDateNode).child(appointment.appointmentID)
        appointmentReference.updateChildValues(appointment.convertToDictionary(), withCompletionBlock: {(error:Error?, ref:FIRDatabaseReference) -> Void in
            if error == nil {
                completionFunction(true, nil)
            }
            else {
                completionFunction(false,error)
            }
        })
        
    }
    
    func deleteAnAppointment(appointment:Appointment, completionFunction:@escaping BoolReturnFunction){
        LocalNotification.shared.deleteNotificationOf(appointment: appointment)
        dbService.deleteAppointment(appointment: appointment) { (success:Bool, error:Error?) in
            if error == nil && success {
                print("Appointment was deleted successfully on DB")
            }
            else {
                print("Appointment couldn't be deleted on DB\n\(error!.localizedDescription)")
            }
            
        }
        self.appointmentsRef.child(currentDentist.dentistID!).child(appointment.appointmentID).removeValue(completionBlock: {(error:Error?, ref:FIRDatabaseReference) -> Void in
            if error == nil {
                completionFunction(true, nil)
            }
            else {
                completionFunction(false, error)
            }
        })
    }
    
    func getPatientAppointments(patient:Patient, completionFunction:@escaping AppointmentArrayReturnFunction){
        var appointmentArray = [Appointment]()
        self.appointmentsRef.child(currentDentist.dentistID!).observeSingleEvent(of: .value) { (snapshot:FIRDataSnapshot) in
            if let childNodes = snapshot.children.allObjects as? [FIRDataSnapshot] {
                for childNode in childNodes {
                    let appointmentNodes = childNode.value as! NSDictionary
                    let appointments = appointmentNodes.allValues as! [NSDictionary]
                    for appointment in appointments {
                        let tempAppointment = Appointment.initAppointmentWith(dictionary: appointment)
                        if tempAppointment.patientID == patient.patientID {
                            appointmentArray.append(tempAppointment)
                        }
                    }
                }
                
            }else {
                print("There is no appointment for this patient")
            }
            completionFunction(appointmentArray, nil)
        }
    }
    
    func getDentistAppointmentForDateInterval(dentist:Dentist, datesArrayInString:[String], completionFunction:@escaping AppointmentArrayReturnFunction){
        var appointmentArray = [Appointment]()        
        self.appointmentsRef.child(currentDentist.dentistID!).observeSingleEvent(of: .value, with: {(snapshot:FIRDataSnapshot) in
            for date in datesArrayInString {
                let fireDate = DateConversion.sharedInstance.convertDBDateToFireDateFormat(date: date)
                if snapshot.hasChild(fireDate) {
                    let dateSnapshot = snapshot.childSnapshot(forPath: fireDate)
                    let value = dateSnapshot.value as! NSDictionary
                    let appointments = value.allValues as! [NSDictionary]
                    for appointment in appointments {
                        appointmentArray.append(Appointment.initAppointmentWith(dictionary: appointment))
                    }
                }
            }
            completionFunction(appointmentArray,nil)
        })
    }
    
    func getAppointmentsForTheFirstLaunch(dentist:Dentist, completionFunction:@escaping AppointmentArrayReturnFunction){
        var appointmentArray = [Appointment]()
        self.appointmentsRef.child(currentDentist.dentistID!).observeSingleEvent(of: .value) { (snapshot:FIRDataSnapshot) in
            if let childNodes = snapshot.children.allObjects as? [FIRDataSnapshot] {
                for childNode in childNodes {
                    let appointmentNodes = childNode.value as! NSDictionary
                    let appointments = appointmentNodes.allValues as! [NSDictionary]
                    for appointment in appointments {
                        let tempAppointment = Appointment.initAppointmentWith(dictionary: appointment)
                        appointmentArray.append(tempAppointment)
                        self.dbService.insertDatas(dbName: .AppointmentDatabase, datas: tempAppointment.configureAppointmentForDB(), completionFunction: { (success:Bool, dbError:Error?) in
                            if dbError == nil && success {
                                print("Appointment was added to DB")
                            }
                        })
                    }
                }
            }else {
                print("The financial hist array is empty")
            }
            completionFunction(appointmentArray,nil)
        }
    }
    
    
    //MARK: Finance Related Functions
    
    func getPatientFinancialStatus(patient:Patient, completionFunction:@escaping FinanceReturnFunction){
        self.fireRootRef.child("finance").child(patient.patientFinancialStatID).observeSingleEvent(of: .value) { (snapshot:FIRDataSnapshot) in
            if let value = snapshot.value as? NSDictionary {
                completionFunction(Finance.initFinanceWith(dictionary: value), nil)
            }
            else {
                print("There is no finance object belonging to the patient")
            }
        }
    }
    
    func updateFinancialStatusOf(patient:Patient, finance:Finance, completionFunction:@escaping BoolReturnFunction){
        let financeRootRef = self.fireRootRef.child("finance").child(patient.patientFinancialStatID)
        financeRootRef.updateChildValues(finance.convertToDict(), withCompletionBlock: {(error:Error?, ref:FIRDatabaseReference) -> Void in
            if error == nil {
                completionFunction(true, nil)
            }
            else {
                print(" - FIRLOG --> update finance object error \(error!.localizedDescription)")
                completionFunction(false,error)
            }
        })
        
    }
    
    func getFinancialHistoryOf(patient:Patient, completionFunction:@escaping FinancialHistoryObjectArrayReturnFunction){
        var financialHistoryArray = [FinancialHistory]()
        self.fireRootRef.child("financialHistory").child(patient.patientID).observeSingleEvent(of: .value) { (snapshot:FIRDataSnapshot) in
            if let childNodes = snapshot.children.allObjects as? [FIRDataSnapshot] {
                for childNode in childNodes {
                    let financialHistories = childNode.value as! NSDictionary
                    let tempHist = FinancialHistory.initWith(dictionary: financialHistories)
                    self.dbService.insertDatas(dbName: .FinancialHistDatabase, datas: tempHist.configureFinancialHistForDB(), completionFunction: { (success:Bool, error:Error?) in
                        if error == nil && success {
                            financialHistoryArray.append(tempHist)
                        }
                        else {
                            print("While inserting financial history object to DB there was an error -> \(error!.localizedDescription)")
                            completionFunction(nil,error!)
                        }
                    })
                }
            }else {
                print("The financial hist array is empty")
            }
            completionFunction(financialHistoryArray,nil)
            
        }
    }
    
    func createFinancialHistoryForPayment(financialHist:FinancialHistory, completionFunction:@escaping BoolReturnFunction){
        let financialHistRef = self.fireRootRef.child("financialHistory").child(financialHist.patientID).childByAutoId()
        financialHist.financialHistoryID = financialHistRef.key
        
        dbService.insertDatas(dbName: .FinancialHistDatabase, datas: financialHist.configureFinancialHistForDB()) { (success:Bool, error:Error?) in
            if error == nil && success {
                print("Financial history object was created successfully on DB")
            }
            else {
                print("Financial history object couldn't be created on DB\n\(error!.localizedDescription)")
            }
        }
        
        financialHistRef.setValue(["financialHistoryID":financialHist.financialHistoryID, "paidAmount":financialHist.paidAmount, "patientID":financialHist.patientID, "paymentDate":financialHist.paymentDate.toString], withCompletionBlock: {(error:Error?, ref:FIRDatabaseReference) -> Void in
            if error == nil {
                completionFunction(true, nil)
            }
            else {
                completionFunction(false, error)
            }
        })
        
    }
    
    
    //MARK: Helper Functions
    
    func createFinanceObjectForPatient(patient:Patient) {
        let newFinance = Finance()
        let financeRootRef = self.fireRootRef.child("finance").child(patient.patientFinancialStatID)
        newFinance.financeID = financeRootRef.key
        
        dbService.insertDatas(dbName: .FinanceDatabase, datas: newFinance.configureFinanceForDB()) { (success:Bool, error:Error?) in
            if error == nil && success {
                print("Finance object was created successfully on DB")
            }
            else {
                print("Finance object couldn't be created on DB\n\(error!.localizedDescription)")
            }
        }
        
        financeRootRef.setValue(["financeID":newFinance.financeID, "totalCost":newFinance.totalCost, "paidAmount":newFinance.paidAmount, "balance":newFinance.balance, "isThereAnyPayment":newFinance.isThereAnyPayment.getIntegerValue, "currency":newFinance.currency,"financeNotes":newFinance.financeNotes,"lastPaymentDate":newFinance.lastPaymentDate.toString])
    }
    
    func saveImageURLOfPatient(patient:Patient, url:URL, isRadiography:Bool) {
        var imageRef:FIRDatabaseReference!
        if isRadiography{
            imageRef = radiographyRef.child(patient.patientID).childByAutoId()
        }
        else {
            imageRef = photosRef.child(patient.patientID).childByAutoId()
        }
        
        imageRef.setValue(["patientID":patient.patientID, "downloadURL":url.absoluteString])
    }
}
