//
//  LoginDentistVC.swift
//  iClinic
//
//  Created by Onur Küçük on 14.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class LoginDentistVC: UIViewController {
    @IBOutlet weak var eMailTextField: UITextField!

    @IBOutlet weak var passwordTextField: UITextField!
    let firebaseService = LoginFunctions()
    override func viewDidLoad() {
        super.viewDidLoad()

        let firstLaunch = FirstLaunch()
        firstLaunch.createAllDatabase()
    }
    
    //MARK: - Backend Service Related Functions
    func loginWithEmailAndPassword(eMail:String, password:String){
        UtilFunctions.sharedInstance.showLoader(viewController: self)
        let dentist = Dentist()
        dentist.dentistEmail = eMail
        dentist.dentistPassword = password
        firebaseService.loginWith(username: eMail, password: password) { (success:Bool, error:Error?) in
            UtilFunctions.sharedInstance.stopActivityIndicator(viewController: self)
            if success == true {
                let firstLaunch = FirstLaunch()
                firstLaunch.insertDatasToDBFromFirebase()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mainVC = storyboard.instantiateInitialViewController()
                self.present(mainVC!, animated: true, completion: nil)
            }
            else {
                let alert = UtilFunctions.sharedInstance.showAlertController(withTitle: "Hata", andMessage: error!.localizedDescription)
                self.present(alert, animated: true, completion: nil)
            }

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginButton(_ sender: UIButton) {
        if eMailTextField.text != "" && passwordTextField.text != "" {
            loginWithEmailAndPassword(eMail: eMailTextField.text!, password: passwordTextField.text!)
        }
        else {
            let alert = UtilFunctions.sharedInstance.showAlertController(withTitle: "Uyarı", andMessage: "E-Posta ve şifre boş bırakılamaz.")
            self.present(alert, animated: true, completion: nil)
        }
    }
}
