//
//  FinancialHistory.swift
//  iClinic
//
//  Created by Onur Küçük on 26.12.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit
import FMDB

class FinancialHistory: NSObject {
    var financialHistoryID = ""
    var patientID = ""
    var paidAmount = 0
    var paymentDate = Date()

    static func initWith(dictionary:NSDictionary) -> FinancialHistory {
        let financialHist = FinancialHistory()
        financialHist.financialHistoryID = dictionary.value(forKey: "financialHistoryID") as! String
        financialHist.patientID = dictionary.value(forKey: "patientID") as! String
        financialHist.paidAmount = dictionary.value(forKey: "paidAmount") as! Int
        financialHist.paymentDate = (dictionary.value(forKey: "paymentDate") as! String).convertToDate
        return financialHist
    }

    static func initFinancialHistResultSet(resultSet:FMResultSet) -> FinancialHistory {
        let financialHist = FinancialHistory()
        financialHist.financialHistoryID = resultSet.string(forColumn: "ID")
        financialHist.patientID = resultSet.string(forColumn: "PATIENT_ID")
        financialHist.paidAmount = (Int)(resultSet.int(forColumn: "PAID_AMOUNT"))
        financialHist.paymentDate = DateConversion.sharedInstance.convertDBDateToDate(dateInString: resultSet.string(forColumn: "PAYMENT_DATE"))
        return financialHist
    }
    
    
    func configureFinancialHistForDB() -> [String]{
        var stringArray = [String]()
        stringArray.append("\(self.financialHistoryID)")
        stringArray.append("\(self.patientID)")
        stringArray.append("\(self.paidAmount)")
        stringArray.append("\(DateConversion.sharedInstance.convertDateToDBFormat(date: self.paymentDate as Date))")
        return stringArray
    }
}
