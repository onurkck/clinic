//
//  PatientDetailVC.swift
//  iClinic
//
//  Created by Onur Küçük on 10.12.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class PatientDetailVC: BaseVC, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var currentPatient:Patient!
    var patientObj:Patient!
    var patientFinancialStatus:Finance!
    var isRadiography = true
    private var totalAmountTextfield:UITextField!
    private var paidAmountTextfield:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "\(currentPatient.patientName) \(currentPatient.patientSurname)"
    }

    override func viewWillAppear(_ animated: Bool) {
        if patientObj == nil {
            self.getPatientObject()
        }
        self.dbService?.getPatientFinanceStatus(patient: currentPatient, completionFunction: { (returned:Finance?, error:Error?) in
            if error == nil {
                self.patientFinancialStatus = returned!
            }
            else {
                self.showAlertController(withTitle: "Hata", andMessage: "Hastanın finansal durumu alınamadı.")
                print("\nFINANCE OBJECT COULDN'T FETCHED BECAUSE OF --->>> \(error!.localizedDescription)")
            }
        })
    }
    //MARK: Backend Related
    func updateFinancialStatus(){
        fireService?.updateFinancialStatusOf(patient:currentPatient, finance: self.patientFinancialStatus, completionFunction: { (success:Bool, error:Error?) in
            if success && error == nil {
                self.dbService?.updateFinanceStatusOfPatient(patient: self.currentPatient, financeObj: self.patientFinancialStatus, completionFunction: { (dbsuccess:Bool, dberror:Error?) in
                    if !dbsuccess{
                        self.showAlertController(withTitle: "Hata", andMessage: "Finansal durum güncellenemedi !")
                        print("\nWHILE UPDATING FINANCIAL STATUS AN ERROR OCCURED !!!!! ------> \(String(describing: error))")
                    }
                })
            }
            else {
                self.showAlertController(withTitle: "Hata", andMessage: "Finansal durum güncellenemedi !")
                print("\nWHILE UPDATING FINANCIAL STATUS AN ERROR OCCURED !!!!! ------> \(String(describing: error))")
            }
        })
    }
    
    func getPatientObject(){
        
    }
    
    func createFinancialHistory(){
        let newFinancialHistObj = FinancialHistory()
        newFinancialHistObj.patientID = self.patientObj.patientID
        newFinancialHistObj.paidAmount = self.patientFinancialStatus.paidAmount
        newFinancialHistObj.paymentDate = Date()
        self.fireService?.createFinancialHistoryForPayment(financialHist: newFinancialHistObj, completionFunction: { (success:Bool, error:Error?) in
            if success {
                print("\nFINANCIAL HISTORY OBJECT CREATED SUCCESSFULLY")
            }
            else {
                print("\nTHERE WAS AN ERROR WHILE CREATEING NEW FINANCIAL HISTORY OBJECT\n\(error!.localizedDescription)")
            }
        })
    }
    
    
    func deleteThePatient(){
        fireService?.deletePatient(patient: currentPatient, completionFunction: { (success:Bool, error:Error?) in
            if success == true {
                print("\nPATIENT DELETED FROM FIREBASE SUCCESSFULLY")
            }
            else {
                print("\nPATIENT COULDN'T DELETED FROM FIREBASE")
            }
        })
        dbService?.deletePatient(patient: currentPatient, completionFunction: { (success:Bool, error:Error?) in
            if success == true {
                _  = self.navigationController?.popViewController(animated: true)
                print("\nPATIENT DELETED FROM DB SUCCESSFULLY")
            }
            else {
                print("\nPATIENT COULDN'T DELETED FROM DB")
            }
        })
    }
    
    //MARK: Table View Related
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PatientDetailCell", for: indexPath) as! PatientDetailTableViewCell
            cell.configureCellWith(patient: self.currentPatient, financialStatus: patientFinancialStatus)
            self.totalAmountTextfield = cell.totalAmountTextField
            self.paidAmountTextfield = cell.paidAmountTextField
            return cell
        }
        else if indexPath.section == 1 {
//            if indexPath.row != 5 {
                let featureCell = tableView.dequeueReusableCell(withIdentifier: "PatientFeatureCell", for: indexPath)
                switch indexPath.row {
                case 0:
                    featureCell.textLabel?.text = "Genel Diş Durumu"
                case 1:
                    featureCell.textLabel?.text = "Hasta Filmleri"
                case 2:
                    featureCell.textLabel?.text = "Hasta Fotoğrafları"
                case 3:
                    featureCell.textLabel?.text = "Randevu Geçmişi"
                case 4:
                    featureCell.textLabel?.text = "Finansal Geçmiş"
                case 5:
                    featureCell.textLabel?.text = "Hasta Bilgilerini Düzenle"
                default:
                    return featureCell
                }
//            }
//            else {
//                return tableView.dequeueReusableCell(withIdentifier: "DeletePatientCell", for: indexPath)
//            }
        }
        return tableView.dequeueReusableCell(withIdentifier: "PatientFeatureCell", for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                if let patient = patientObj {
                    self.showTeethGraph(ofPatient: patient)
                }
            case 1:
                self.isRadiography = true
                self.performSegue(withIdentifier: "showPatientPhotos", sender: self)
            case 2:
                self.isRadiography = false
                self.performSegue(withIdentifier: "showPatientPhotos", sender: self)
            case 3:
                self.performSegue(withIdentifier: "showAppointmentHistory", sender: self)
            case 4:
                self.performSegue(withIdentifier: "showFinancialHistory", sender: self)
            case 5:
//                askDeletingThePatient()
                print("Go to edit screen")
            default:
                print("")
            }
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 132.0
        }
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "FINANS DETAYLARI"
        }
        return "HASTA DETAYLARI"
    }
    
    //MARK: Action
    @IBAction func updatePatientFinancialStat(_ sender: UIButton) {
        
//        let enteredTotalAmount = totalAmountTextfield.getIntegerValue
//        let enteredPaidAmount = paidAmountTextfield.getIntegerValue
//        let isFinanceNoteChanged = patientFinancialStatus.financeNotes != self.financialnote
//        let isNewTotalAmountEntered = patientFinancialStatus.totalCost != enteredTotalAmount
//        let isPaidAmountEntered = enteredPaidAmount != 0
//        
//        if isPaidAmountEntered || isNewTotalAmountEntered || isFinanceNoteChanged {
//            if isNewTotalAmountEntered && isPaidAmountEntered {
//                self.patientFinancialStatus.balance = self.patientFinancialStatus.balance + (enteredTotalAmount - self.patientFinancialStatus.totalAmount) - enteredPaidAmount
//                self.patientFinancialStatus.paidAmount = enteredPaidAmount
//                self.patientFinancialStatus.totalAmount = enteredTotalAmount
//                self.patientFinancialStatus.lastPaymentDate = UtilFunctions.sharedInstance.convertDateToDBFormat(date: Date())
//                self.createFinancialHistory()
//            }
//            else if !isNewTotalAmountEntered && isPaidAmountEntered {
//                self.patientFinancialStatus.balance = self.patientFinancialStatus.balance - enteredPaidAmount
//                self.patientFinancialStatus.paidAmount = enteredPaidAmount
//                self.patientFinancialStatus.lastPaymentDate = UtilFunctions.sharedInstance.convertDateToDBFormat(date: Date())
//                self.createFinancialHistory()
//            }
//            else if isNewTotalAmountEntered && !isPaidAmountEntered {
//                self.patientFinancialStatus.balance = self.patientFinancialStatus.balance + (enteredTotalAmount - self.patientFinancialStatus.totalAmount)
//                self.patientFinancialStatus.totalAmount = enteredTotalAmount
//            }
//            else if isFinanceNoteChanged {
//                self.patientFinancialStatus.financeNotes = self.financialNoteTextView.text
//            }
//            updateFinancialStatus()
//            tableView.reloadData()
//        }
    }
    
    func createAppointmentOfCurrentPatient(){
        self.performSegue(withIdentifier: "createAppointment", sender: self)
    }
    
    func askDeletingThePatient(){
        let alertController = UIAlertController(title: "Uyarı", message: "Hastayı silmek istediğnizden emin misiniz? Bu işlem geri alınamaz ve hastayla ilgili tüm verileri (Randevu Geçmişi, Hasta Filmleri, Hasta Fotoğrafları, Finansal Durum) silmiş olursunuz.", preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: "Evet", style: .destructive, handler: { (act) in
            self.deleteThePatient()
        })
        let cancelAction = UIAlertAction(title: "Vazgeç", style: .cancel, handler: nil)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    // MARK: - Navigation
    
    
    func showTeethGraph(ofPatient:Patient){
        let teetGraphVC = self.storyboard?.instantiateViewController(withIdentifier: "TeethGraph") as! TeethGraphVC
        teetGraphVC.selectedPatient = self.patientObj
        self.present(teetGraphVC, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPatientPhotos" {
            let destVC = segue.destination as! ImagePreviewCollectionVC
            destVC.isRadiography = isRadiography
            destVC.currentPatient = self.currentPatient
        }
        else if segue.identifier == "showAppointmentHistory" {
            self.dbService?.getAppointmentHistoryOfPatient(patient: self.currentPatient, completionFunction: { (returnedAppointments:[Appointment]?, error:Error?) in
                if error == nil && returnedAppointments != nil {
                    let destVC = segue.destination as! PatientAppointmentHistoryVC
                    destVC.appointmentHistory = returnedAppointments!
                    destVC.currentPatient = self.currentPatient
                    destVC.navigationItem.title = "\(self.currentPatient.patientName) \(self.currentPatient.patientSurname)"
                }
                else {
                    print("\nTHERE WAS AN ERROR WHILE FETCHING APPOINTMENT HISTORY")
                }
            })
        }
        else if segue.identifier == "createAppointment" {
            let destVC = segue.destination as! CreateAppointmentVC
            destVC.selectedPatient = self.currentPatient
        }
        else if segue.identifier == "showFinancialHistory" {
            self.dbService?.getPatientFinancialHistory(patient: self.currentPatient, completionFunction: { (returnedHist:[FinancialHistory]?, error:Error?) in
                if error == nil {
                    let destVC = segue.destination as! FinancialHistoryVC
                    destVC.financialHistOfUser = returnedHist!
                }
                else {
                    print("\nTHERE WAS AN ERROR WHILE FETCHING FINANCIAL HISTORY")
                }
            })
        }
    }
}
