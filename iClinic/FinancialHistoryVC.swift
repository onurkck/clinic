//
//  FinancialHistoryVC.swift
//  iClinic
//
//  Created by Onur Küçük on 25.12.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class FinancialHistoryVC: BaseVC, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView:UITableView!
    var financialHistOfUser = [FinancialHistory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sortPaymentHistory()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if financialHistOfUser.count != 0 {
            return financialHistOfUser.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if financialHistOfUser.count == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "NoPaymentCell", for: indexPath)
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath) as! PaymentTableViewCell
        cell.configureCellWithDBFinancialHist(financialHist: financialHistOfUser[indexPath.row])
        return cell
    }
    
    func sortPaymentHistory(){
        financialHistOfUser.sort(by: {($0.paymentDate as Date) > ($1.paymentDate as Date)})
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
