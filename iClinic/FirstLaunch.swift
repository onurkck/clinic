//
//  FirstLaunch.swift
//  iClinic
//
//  Created by Onur Küçük on 22.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class FirstLaunch: NSObject {
    
    var dbService:DBServices = DBServicesImplementation()
    var fireService:FirebaseService = FirebaseImplementation()
    func createAllDatabase(){
        dbService.createTablesWith(dbName: .PatientDatabase, columnsAndTypes: Statics.patientColumnsAndTypes, completionFunction: { (status:Bool, error:Error?) in
            print("CREATING PATIENT DB TABLE STATUS IS --> \(status)")
            if error != nil {
                print("THERE WAS AN ERROR WHILE CREATING PATIENT DB TABLE \(error!.localizedDescription)")
            }
        })
        dbService.createTablesWith(dbName: .AppointmentDatabase, columnsAndTypes: Statics.appointmentColumnsAndTypes, completionFunction: { (status:Bool, error:Error?) in
            print("CREATING APPOINTMENT DB TABLE STATUS IS --> \(status)")
            if error != nil {
                print("THERE WAS AN ERROR WHILE CREATING APPOINTMENT DB TABLE \(error!.localizedDescription)")
            }
        })
        dbService.createTablesWith(dbName: .FinanceDatabase, columnsAndTypes: Statics.financeColumnsAndTypes, completionFunction: { (status:Bool, error:Error?) in
            print("CREATING FINANCE DB TABLE STATUS IS --> \(status)")
            if error != nil {
                print("THERE WAS AN ERROR WHILE CREATING FINANCE DB TABLE \(error!.localizedDescription)")
            }
        })
        dbService.createTablesWith(dbName: .FinancialHistDatabase, columnsAndTypes: Statics.financialHistoryColumnsAndTypes) { (status:Bool, error:Error?) in
            if error != nil {
                print("THERE WAS AN ERROR WHILE CREATING FINANCIAL HISTORY DB TABLE \(error!.localizedDescription)")
            }
        }
    }
    
    func insertDatasToDBFromFirebase(){
        
        DispatchQueue.global().sync {
            fireService.getDentistPatients(dentist: currentDentist) { (returnedPatients:[Patient]?, error:Error?) in
                if error == nil {
                    for patient in returnedPatients! {
                        
                        self.dbService.insertDatas(dbName: .PatientDatabase, datas: patient.configurePatientForDB(), completionFunction: { (success:Bool, dbError:Error?) in
                            if dbError == nil {
                                print("INSERTING PATIENT DB STATUS: \(success)")
                            }
                            else {
                                print("AN ERROR OCCURED WHILE PATIENTS ADDING TO DB")
                            }
                        })
                        
                        DispatchQueue.global().sync {
                            self.fireService.getFinancialHistoryOf(patient: patient, completionFunction: { (returnedFinancialHistory:[FinancialHistory]?, fError:Error?) in
                                if fError == nil {
                                    for financialHist in returnedFinancialHistory! {
                                        self.dbService.insertDatas(dbName: .FinancialHistDatabase, datas: financialHist.configureFinancialHistForDB(), completionFunction: { (success:Bool, dbError:Error?) in
                                            if dbError == nil {
                                                print("INSERTING FINANCIAL HISTORY TO DB STATUS: \(success)")
                                            }
                                            else {
                                                print("AN ERROR OCCURED WHILE FINANCIAL HISTORY ADDING TO DB")
                                            }
                                        })
                                    }
                                }
                                else{
                                    print("AN ERROR OCCURED WHILE FINANCIAL HISTORY FETCHING")
                                }
                            })
                        }
                        DispatchQueue.global().sync {
                            self.fireService.getPatientFinancialStatus(patient: patient, completionFunction: { (returned:Finance?, error:Error?) in
                                if error == nil, let finance = returned {
                                    self.dbService.insertDatas(dbName: .FinanceDatabase, datas: finance.configureFinanceForDB(), completionFunction: { (success:Bool, dbError:Error?) in
                                        if dbError == nil {
                                            print("INSERTING FINANCE TO DB STATUS: \(success)")
                                        }
                                        else {
                                            print("AN ERROR OCCURED WHILE FINANCE ADDING TO DB")
                                        }
                                    })
                                }
                            })
                        }
                    }
                }
                print("AN ERROR OCCURED WHILE PATIENTS FETCHING")
            }
        }
        
        DispatchQueue.global().sync {
            fireService.getAppointmentsForTheFirstLaunch(dentist: currentDentist) { (_:[Appointment]?, error:Error?) in
                if error == nil {
                    print("Appointments fetched and added successfully")
                }
            }
        }
    }
}
