//
//  Animation.swift
//  iClinic
//
//  Created by Onur Küçük on 27.02.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import UIKit

extension UICollectionView {
    func addBlurEffect(){
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
    
    func removeBlurEffect(){
        self.subviews.last?.removeFromSuperview()
    }
}

class Animation{
    
    static let sharedInstance = Animation()
    
    
    func addBlurEffectTo(views:[UIView], withViews:[UIView]) {
        var overlays = [UIVisualEffectView]()
        for view in views {
            let overlay = UIVisualEffectView(frame: view.bounds)
            overlay.effect = nil
            overlays.append(overlay)
        }
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3) {
                for containerView in withViews {
                    containerView.alpha = 1.0
                }
                for overlay in overlays {
                    overlay.effect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                 }
            }
            for i in 0...views.count - 1 {
                views[i].addSubview(overlays[i])
            }
            for containerView in withViews {
                containerView.isHidden = false
            }
        }
    }
    
    func removeBlurEffectFrom(views:[UIView], withViews:[UIView]){
        var overlays = [UIVisualEffectView]()
        for view in views {
            let overlay = view.subviews.last as! UIVisualEffectView
            overlays.append(overlay)
        }
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, animations: {
                for containerView in withViews {
                    containerView.alpha = 0.0
                }
                
                for overlay in overlays {
                    overlay.alpha = 0.0
                }
                
            }) { (result) in
                for containerView in withViews {
                    containerView.isHidden = true
                }
                for overlay in overlays {
                    overlay.removeFromSuperview()
                }
            }
        }
    }
}

//protocol ContainerViewTouched {
//    func selectedContainerView()
//}
//
//enum ContainerViewName:String {
//    case AddPatientCV = "AddPatient"
//    case MyPatientsCV = "MyPatients"
//    case AddAppointmentCV = "AddAppointment"
//}
//
//class ContainerView:UIView {
//    var containerType:ContainerViewName!
//    var delegate:ContainerViewTouched?
//
//    init(containerType:) {
//        <#statements#>
//    }
//}
