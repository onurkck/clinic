//
//  LoginFunctions.swift
//  iClinic
//
//  Created by Onur Küçük on 26.05.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import UIKit
import Firebase
class LoginFunctions: NSObject {

    var fireRootRef = FIRDatabase.database().reference()

    //MARK: Login & SignUp
    
    func signUpDentist(dentist:Dentist, completionFunction:@escaping BoolReturnFunction)
    {
        FIRAuth.auth()?.createUser(withEmail: dentist.dentistEmail, password: dentist.dentistPassword, completion: { (returned:FIRUser?, error:Error?) in
            if error == nil, let registeredUser = returned {
                currentDentist = Dentist.initFIRUser(object: registeredUser)
                self.fireRootRef.child("user").child(currentDentist.dentistID!).setValue(["dentistID":currentDentist.dentistID!, "dentistName":dentist.dentistName, "dentistSurname":dentist.dentistSurname, "dentistPhone":dentist.dentistPhone, "dentistEmail":dentist.dentistEmail, "dentistClinicID":"123"], withCompletionBlock: {(err:Error?, ref:FIRDatabaseReference) -> Void in
                    if err == nil {
                        completionFunction(true, nil)
                    }
                    else {
                        completionFunction(false, err)
                    }
                })
            }
            else {
                print(" - FIRLOG --> sign up user error \(error!.localizedDescription)")
                completionFunction(false, error)
            }
        })
    }
    
    func loginWith(username:String, password:String, completionFunction:@escaping BoolReturnFunction)
    {
        FIRAuth.auth()?.signIn(withEmail: username, password: password, completion: { (returned:FIRUser?, error:Error?) in
            if error == nil, let registeredUser = returned {
                currentDentist = Dentist.initFIRUser(object: registeredUser)
                
                // TODO: Login event comes here - Firebase Analytics -
                completionFunction(true, nil)
            }
            else {
                print(" - FIRLOG --> sign in user error \(error!.localizedDescription)")
                completionFunction(false, error)
            }
        })
    }
    
}
