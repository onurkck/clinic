//
//  PatientImageTableViewCell.swift
//  iClinic
//
//  Created by Onur Küçük on 26.12.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class PatientImageTableViewCell: UITableViewCell {
    @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
    @IBOutlet weak var patientPhotoPreview:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureWith(patientPhoto:PatientPhoto) {
//        let fireService:Services = ServicesImplementation()
//        fireService.getImageOf(pffile: patientPhoto.imagePffile) { (imageData:Data?, error:Error?) in
//            if error == nil {
//                UIView.animate(withDuration: 0.3, animations: {
//                    self.patientPhotoPreview.alpha = 1
//                    self.activityIndicator.stopAnimating()
//                })
//                self.patientPhotoPreview.image = UIImage(data: imageData!)
//            }
//            else {
//                self.activityIndicator.stopAnimating()
//            }
//        }
    }

}
