//
//  ImagePickerVC.swift
//  iClinic
//
//  Created by Onur Küçük on 18.12.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

protocol ImageAddingDelegate {
    func imageAdded()
}

class ImagePickerVC: BaseVC, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imagePreview: UIImageView!
    let picker = UIImagePickerController()
    var choosenImage:UIImage?
    var forPatient:Patient!
    var delegate:ImageAddingDelegate?
    var isRadiographyPhoto = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Backend Related
    
    func saveSelectedImage(){
        showActivityIndicator()
        fireService?.uploadPatientPhoto(patient: forPatient, image: choosenImage!, isRadiography: isRadiographyPhoto, completionFunction: { (success:Bool, error:Error?) in
            self.stopActivityIndicator()
            if success && error == nil {
                self.delegate?.imageAdded()
                self.dismiss(animated: true, completion: nil)
            }
            else {
                self.stopActivityIndicator()
                self.showAlertController(withTitle: "Hata", andMessage: "Fotoğraf yüklenirken beklenmedik bir hata oluştu, lütfen tekrar deneyiniz.")
            }
        })
    }
    
    //MARK: Actions
    
    @IBAction func pickFromGallery(_ sender: UIButton) {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func captureWithCamera(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            print("The camera is not available")
        }
    }
    
    @IBAction func saveImage(_ sender: UIButton) {
        if choosenImage != nil {
            saveSelectedImage()
        }
        else {
            self.showAlertController(withTitle: "", andMessage: "Lütfen bir fotoğraf seçiniz/çekiniz.")
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Delegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        choosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        imagePreview.contentMode = .scaleAspectFit
        imagePreview.image = choosenImage
        dismiss(animated: true, completion: {
            self.animateImageView()
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Helper
    func animateImageView(){
        UIView.animate(withDuration: 0.5, animations: {
            self.imagePreview.alpha = 1
        })
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
