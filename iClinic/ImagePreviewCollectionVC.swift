//
//  ImagePreviewCollectionVC.swift
//  iClinic
//
//  Created by Onur Küçük on 9.03.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import UIKit


class ImagePreviewCollectionVC: BaseVC, UICollectionViewDelegate, UICollectionViewDataSource,ImageAddingDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var noImageLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var patientPhotoArray = [PatientPhoto]()
    var currentPatient:Patient!
    var isRadiography:Bool!
    var selectedImage:UIImage!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getPatientPhotos()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addPhoto))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Backend Related
    func getPatientPhotos(){
        fireService?.getPatientPhotos(patient: currentPatient, isRadiography: isRadiography) { (returnedPhotos:[PatientPhoto]?, error:Error?) in
            if error == nil {
                self.patientPhotoArray = returnedPhotos!
                if self.patientPhotoArray.count == 0 {
                    self.showNoImageLabel()
                }
                self.collectionView?.reloadData()
            }
            else {
                self.present(UtilFunctions.sharedInstance.showAlertController(withTitle: "Hata", andMessage: "Beklenmedik bir hata gerçekleşti lütfen daha sonra tekrar deneyin."), animated: true, completion: nil)
            }
        }
    }

    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if patientPhotoArray.count > 2 {
            return patientPhotoArray.count/2
        }
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if patientPhotoArray.count == 0 {
            return 0
        }
        if patientPhotoArray.count%2 == 0 {
            return 2
        }
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagePreviewCell", for: indexPath) as! ImageCollectionViewCell
        if patientPhotoArray.count == 0 {
           return self.configureCellForNoImage(cell: cell)
        }
        
        let arrayIndex = indexPath.section * 2 + indexPath.row
        cell.configureCellWith(patientPhoto: patientPhotoArray[arrayIndex])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width: (self.view.frame.size.width - 20)/2, height: (self.view.frame.size.width - 20)/2)
    }
    
    func configureCellForNoImage(cell:ImageCollectionViewCell) -> ImageCollectionViewCell{
        cell.activityIndicator.stopAnimating()
        cell.photoView.image = #imageLiteral(resourceName: "noImage")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ImageCollectionViewCell
        self.selectedImage = cell.photoView.image!
        self.performSegue(withIdentifier: "showImageDetail", sender: self)
    }
    
    //MARK: Helper
    func imageAdded() {
        self.getPatientPhotos()
    }
    
    func addPhoto(){
        let destVC = self.storyboard?.instantiateViewController(withIdentifier: "ImagePickerVC") as! ImagePickerVC
        destVC.forPatient = currentPatient
        destVC.delegate = self
        destVC.isRadiographyPhoto = self.isRadiography
        _ = self.present(destVC, animated: true, completion: nil)
    }
    
    func showNoImageLabel(){
        Animation.sharedInstance.addBlurEffectTo(views: [self.collectionView], withViews: [noImageLabel])
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showImageDetail" {
            let destVC = segue.destination as! ImageDetailVC
            destVC.selectedImage = self.selectedImage
        }
    }
}
