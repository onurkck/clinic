//
//  ImageCollectionViewCell.swift
//  iClinic
//
//  Created by Onur Küçük on 11.03.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
    private var fireService:FirebaseService = FirebaseImplementation()
    
    func configureCellWith(patientPhoto:PatientPhoto) {
        self.activityIndicator.startAnimating()
        fireService.download(patientPhoto: patientPhoto) { (image:UIImage?, error:Error?) in
            self.activityIndicator.stopAnimating()
            if error == nil {
                self.showImageWithAnimation(image: image!)
            }
        }
    }
    
    
    func downloadFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        let url = URL(string:link)
        contentMode = mode
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
            }.resume()
    }
    
    func showImageWithAnimation(image:UIImage){
        UIView.animate(withDuration: 0.2, animations: {
            self.photoView.alpha = 0
        })
        UIView.animate(withDuration: 0.2, animations: {
            self.photoView.image = image
            self.photoView.alpha = 1
        })
    }
}
