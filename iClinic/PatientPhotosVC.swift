//
//  PatientPhotosVC.swift
//  iClinic
//
//  Created by Onur Küçük on 26.12.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class PatientPhotosVC:BaseVC, UITableViewDataSource, UITableViewDelegate,ImageAddingDelegate {
    @IBOutlet weak var tableView:UITableView!
    var patientPhotoArray = [PatientPhoto]()
    var currentPatient:DBPatient!
    var isRadiography:Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addRadiographyImage))
        if checkInternetConnection() {
            self.getPatientPhotos()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Backend Related
    
    func getPatientPhotos(){
        showLoader()
        parseService?.getPatientPhotos(patient: currentPatient, isRadiography: isRadiography, completionFunction: { (returned:[PatientPhoto]?, error:Error?) in
            self.stopActivityIndicator()
            if error == nil && returned != nil {
                self.patientPhotoArray = returned!
                self.tableView.reloadData()
            }
            else {
                self.showAlertController(withTitle: "Hata", andMessage: "Beklenmedik bir hata gerçekleşti lütfen daha sonra tekrar deneyin.")
                print(error!.localizedDescription)
            }
        })
    }
    
    //MARK: Table View Related
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if patientPhotoArray.count > 0 {
            return patientPhotoArray.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if patientPhotoArray.count == 0 {
            if isRadiography == true {
                return tableView.dequeueReusableCell(withIdentifier: "NoRadiography", for: indexPath)
            }
            else {
                return tableView.dequeueReusableCell(withIdentifier: "NoPatientPhoto", for: indexPath)
            }
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientPhotoCell", for: indexPath) as! PatientImageTableViewCell
        cell.configureWith(patientPhoto: patientPhotoArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if patientPhotoArray.count > 0 {
            return 255.0
        }
        return 44.0
    }
    
    //MARK: Helper & Delegate
    
    func addRadiographyImage(){
        let destVC = self.storyboard?.instantiateViewController(withIdentifier: "ImagePickerVC") as! ImagePickerVC
        destVC.forPatient = currentPatient
        destVC.delegate = self
        destVC.isRadiographyPhoto = self.isRadiography
        _ = self.present(destVC, animated: true, completion: nil)
    }
    
    func imageAdded() {
        self.getPatientPhotos()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
