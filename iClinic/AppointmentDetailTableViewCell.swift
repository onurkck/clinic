//
//  AppointmentDetailTableViewCell.swift
//  iClinic
//
//  Created by Onur Küçük on 12.12.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class AppointmentDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var totalAmountTextField: UITextField!
    @IBOutlet weak var paidAmountTextField: UITextField!

    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var financialNotes: UITextView!
    @IBOutlet weak var lastPaymentLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWith(financialStatus:Finance) {
        self.totalAmountTextField.text = "\(financialStatus.totalCost)"
        self.paidAmountTextField.text = ""
        self.balanceLabel.text = "Kalan: \(financialStatus.balance)"
        self.financialNotes.text = financialStatus.financeNotes
        if financialStatus.paidAmount != 0 {
            self.lastPaymentLabel.textColor = UIColor(colorLiteralRed: 104/255.0, green: 207/255.0, blue: 235/255.0, alpha: 1)
            self.lastPaymentLabel.text = "\(DateConversion.sharedInstance.convertDateToDBFormat(date: financialStatus.lastPaymentDate)) tarihinde \(financialStatus.paidAmount)TL ödendi."
            //TODO: Use currency for paid amount.
        }
        else {
            self.lastPaymentLabel.textColor = UIColor.red
            self.lastPaymentLabel.text = "Henüz ödeme yapılmadı."
        }
    }

}
