//
//  Finance.swift
//  iClinic
//
//  Created by Onur Küçük on 29.10.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit
import FMDB

class Finance: NSObject {

    var financeID = ""
    var totalCost = 0
    var paidAmount = 0 //always shows the last payment
    var balance = 0
    var isThereAnyPayment = false
    var currency = ""
    var financeNotes = ""
    var lastPaymentDate = Date()
    
    var dbPaymentDate = ""
    //TODO: Revise dates !!!

    static func initFinanceWith(dictionary:NSDictionary) -> Finance {
        let finance = Finance()
        finance.financeID = dictionary.value(forKey: "financeID") as! String
        finance.totalCost = dictionary.value(forKey: "totalCost") as! Int
        finance.paidAmount = dictionary.value(forKey: "paidAmount") as! Int
        finance.balance = dictionary.value(forKey: "balance") as! Int
        finance.isThereAnyPayment = (dictionary.value(forKey: "isThereAnyPayment") as! Int).toBool
        finance.currency = dictionary.value(forKey: "currency") as! String
        finance.financeNotes = dictionary.value(forKey: "financeNotes") as! String
        finance.lastPaymentDate = (dictionary.value(forKey: "lastPaymentDate") as! String).convertToDate
        return finance
    }
    
    static func initFinanceResultSet(resultSet:FMResultSet) -> Finance {
        let finance = Finance()
        finance.financeID = resultSet.string(forColumn: "ID")
        finance.totalCost = (Int)(resultSet.int(forColumn: "TOTAL_AMOUNT"))
        finance.paidAmount = (Int)(resultSet.int(forColumn: "PAID_AMOUNT"))
        finance.balance = (Int)(resultSet.int(forColumn: "BALANCE"))
        finance.isThereAnyPayment = finance.balance != 0
        finance.financeNotes = resultSet.string(forColumn: "FINANCE_NOTES")
        if finance.isThereAnyPayment {
            finance.lastPaymentDate = DateConversion.sharedInstance.convertDBDateToDate(dateInString: resultSet.string(forColumn: "LAST_PAYMENT_DATE"))
        }
        else {
            finance.dbPaymentDate = resultSet.string(forColumn: "LAST_PAYMENT_DATE")
        }
        //TODO: Convert date to dd/MM/yyyy format
        return finance
    }

    func convertToDict() -> [String : Any] {
        return ["totalCost":self.totalCost, "paidAmount":self.paidAmount, "balance":self.balance, "isThereAnyPayment":self.isThereAnyPayment.getIntegerValue, "currency":self.currency, "financeNotes":self.financeNotes, "lastPaymentDate":self.lastPaymentDate.toString]
    }

    func configureFinanceForDB() -> [String] {
        var stringArray = [String]()
        stringArray.append("\(self.financeID)")
        stringArray.append("\(self.totalCost)")
        stringArray.append("\(self.paidAmount)")
        stringArray.append("\(self.balance)")
        stringArray.append("\(self.isThereAnyPayment.getIntegerValue)")
        stringArray.append("\(self.financeNotes)")
        if paidAmount != 0 {
            // if there is a last payment, shows updateAt field else show empty string
            stringArray.append("\(DateConversion.sharedInstance.convertDateToDBFormat(date: self.lastPaymentDate as Date))")
        }
        else {
            stringArray.append("")
        }
        return stringArray
    }
}
