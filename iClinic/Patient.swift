//
//  Patient.swift
//  iClinic
//
//  Created by Onur Küçük on 29.10.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit
import FMDB

class Patient: NSObject {
    
    var patientID = ""
    var patientName = ""
    var patientSurname = ""
    var patientPhone = 0
    var patientEmail = ""
    
    var healedTooth = [""]
    var unhealedTooth = [""]
    var removedTooth = [""]
    
    var patientFinancialStatID = ""
    var patientAppointmentHistory:[Appointment]!
    
    var patientFinance:Finance!
    
    
    static func initPatientWith(dictionary:NSDictionary) -> Patient {
        let patient = Patient()
        patient.patientID = dictionary.value(forKey: "patientID") as! String
        patient.patientName = dictionary.value(forKey: "patientName") as! String
        patient.patientSurname = dictionary.value(forKey: "patientSurname") as! String
        patient.patientEmail = dictionary.value(forKey: "patientEmail") as! String
        patient.patientPhone = dictionary.value(forKey: "patientPhone") as! Int
        patient.patientFinancialStatID = dictionary.value(forKey: "patientFinanceID") as! String
        patient.healedTooth = dictionary.value(forKey: "patientHealedToothIDs") as! [String]
        patient.unhealedTooth = dictionary.value(forKey: "patientUnhealedToothIDs") as! [String]
        patient.removedTooth = dictionary.value(forKey: "patientRemovedToothIDs") as! [String]
        return patient
    }
    
    static func initPatientResultSet(resultSet:FMResultSet) -> Patient {
        let patient = Patient()
        patient.patientID = resultSet.string(forColumn: "ID")
        patient.patientName = resultSet.string(forColumn: "NAME")
        patient.patientSurname = resultSet.string(forColumn: "SURNAME")
        patient.patientPhone = resultSet.object(forColumnName: "PHONE") as! Int
        patient.patientEmail = resultSet.string(forColumn: "EMAIL")
        patient.patientFinancialStatID = resultSet.string(forColumn: "FINANCE_ID")
        return patient
    }
    
    func convertToDict() -> [String : Any] {
        return ["patientID":self.patientID,"patientFinanceID":self.patientFinancialStatID,"patientName":self.patientName, "patientSurname":self.patientSurname, "patientEmail":self.patientEmail, "patientPhone":self.patientPhone, "patientHealedToothIDs":self.healedTooth, "patientUnhealedToothIDs":self.unhealedTooth, "patientRemovedToothIDs":self.removedTooth]
    }

     func configurePatientForDB() -> [String] {
        var stringArray = [String]()
        stringArray.append("\(self.patientID)")
        stringArray.append("\(self.patientName)")
        stringArray.append("\(self.patientSurname)")
        stringArray.append("\(self.patientPhone)")
        stringArray.append("\(self.patientEmail)")
        stringArray.append("\(self.patientFinancialStatID)")
        return stringArray
    }
}
