//
//  ImageDetailVC.swift
//  iClinic
//
//  Created by Onur Küçük on 14.03.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import UIKit

class ImageDetailVC: UIViewController, UIScrollViewDelegate{
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var scrollView:UIScrollView!
    var selectedImage:UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.minimumZoomScale = 1
        self.scrollView.maximumZoomScale = 10
        self.scrollView.contentSize = self.imageView.frame.size
        self.imageView.image = selectedImage
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
