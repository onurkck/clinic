//
//  TeethGraphVC.swift
//  iClinic
//
//  Created by Onur Küçük on 12.12.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class TeethGraphVC: BaseVC, UIPopoverPresentationControllerDelegate {
    var selectedPatient = Patient()
    var shouldPatientSynchronize = false
    private var healedToothArray = [String]()
    private var unhealedToothArray = [String]()
    private var removedToothArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareVariables()
    }
    
    //MARK: Update Patient Object
    
    func updatePatient(){
        
        if self.healedToothArray.count == 0 { selectedPatient.healedTooth = [""] }
        else { selectedPatient.healedTooth = self.healedToothArray }
        if self.unhealedToothArray.count == 0 { selectedPatient.healedTooth = [""] }
        else { selectedPatient.unhealedTooth = self.unhealedToothArray }
        if self.removedToothArray.count == 0 { selectedPatient.removedTooth = [""] }
        else{ selectedPatient.removedTooth = self.removedToothArray }
        
        fireService?.updatePatient(patient: selectedPatient, completionFunction: { (success:Bool, err:Error?) in
            if err == nil && success {
                print("Arrays successfully updated")
            }
            else {
                print("There is an error while updating arrays")
            }
        })
    }
    
    //MARK: Action
    
    @IBAction func closeTeethGraph(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            if self.shouldPatientSynchronize {
                self.updatePatient()
            }
        })
    }
    
    @IBAction func selectTeeth(_ sender: UIButton) {
        //show alert controler as popover screen includes unhealed, healed, remove
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let selectForUnhealed = UIAlertAction(title: "Tedavi edilecek", style: .default, handler: { (act) in
            self.addSelectedToothForUnhealed(toothID: sender.tag)
            self.shouldPatientSynchronize = true
            
        })
        
        let selectForHealed = UIAlertAction(title: "Tedavi Tamamlandı", style: .default, handler: {(act) in
            self.addSelectedToothForHealed(toothID: sender.tag)
            self.shouldPatientSynchronize = true
        })
        
        let selectForRemove = UIAlertAction(title: "Kaldır", style: .destructive, handler: {(act) in
            sender.isHidden = true
            sender.isUserInteractionEnabled = false
            self.removeSelectedTooth(toothID: sender.tag)
            self.shouldPatientSynchronize = true
            //TODO : Maybe tooth added with gray color and then the tooth could be added after a treatment and the dentist can add again ??  !!! ** ** * ** * *** ** *** *** *
        })
        
        let cancel = UIAlertAction(title: "Vazgeç", style: .cancel, handler: nil)
        
        alertController.addAction(selectForUnhealed)
        alertController.addAction(selectForHealed)
        alertController.addAction(selectForRemove)
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK: Helper Functions
    
    func prepareTeethForThePatient(){
        for unhealedTooth in self.unhealedToothArray {
            let toothID = (Int)(unhealedTooth)!
            let toothButton = self.view.viewWithTag(toothID) as! UIButton
            toothButton.setImage(UIImage(named: "Unhealed_\(toothID)"), for: .normal)
        }
        
        for healedTooth in self.healedToothArray {
            let toothID = (Int)(healedTooth)!
            let toothButton = self.view.viewWithTag(toothID) as! UIButton
            toothButton.setImage(UIImage(named: "Healed_\(toothID)"), for: .normal)
        }
        
        for removedTooth in self.removedToothArray {
            let toothID = (Int)(removedTooth)!
            let toothButton = self.view.viewWithTag(toothID) as! UIButton
            let toothNameLabel = self.view.viewWithTag(toothID+100) as! UILabel //labels has 1 prefix for their tags
            toothNameLabel.isHidden = true
            toothButton.isHidden = true
            toothButton.isUserInteractionEnabled = false
        }
    }
    
    func addSelectedToothForHealed(toothID:Int){
        if let foundIndex = self.unhealedToothArray.index(where: {$0 == "\(toothID)"}) { //SEARCH IN UNHEALED TEETH
            self.unhealedToothArray.remove(at: foundIndex)
        }
        if !self.healedToothArray.contains("\(toothID)") {
            self.healedToothArray.append("\(toothID)")
        }
        self.prepareTeethForThePatient()
    }
    
    func addSelectedToothForUnhealed(toothID:Int) {
        if let foundIndex = self.healedToothArray.index(where: {$0 == "\(toothID)"}) { //SEARCH IN UNHEALED TEETH
            self.healedToothArray.remove(at: foundIndex)
        }
        if !self.unhealedToothArray.contains("\(toothID)") {
            self.unhealedToothArray.append("\(toothID)")
        }
        self.prepareTeethForThePatient()
    }
    
    func removeSelectedTooth(toothID:Int){
        if let foundIndex = self.healedToothArray.index(where: {$0 == "\(toothID)"}) { //SEARCH IN UNHEALED TEETH
            self.healedToothArray.remove(at: foundIndex)
        }
        if let foundIndex = self.unhealedToothArray.index(where: {$0 == "\(toothID)"}) { //SEARCH IN UNHEALED TEETH
            self.unhealedToothArray.remove(at: foundIndex)
        }
        if !self.removedToothArray.contains("\(toothID)"){
            self.removedToothArray.append("\(toothID)")
        }
    }
    
    func prepareVariables(){
        self.healedToothArray = selectedPatient.healedTooth
        self.unhealedToothArray = selectedPatient.unhealedTooth
        self.removedToothArray = selectedPatient.removedTooth
        if let foundIndex = self.healedToothArray.index(where: {$0 == ""}) {
            self.healedToothArray.remove(at: foundIndex)
        }
        if let foundIndex = self.unhealedToothArray.index(where: {$0 == ""}) {
            self.unhealedToothArray.remove(at: foundIndex)
        }
        if let foundIndex = self.removedToothArray.index(where: {$0 == ""}) {
            self.removedToothArray.remove(at: foundIndex)
        }
        self.prepareTeethForThePatient()
    }
}
