//
//  SignUpTableVC.swift
//  iClinic
//
//  Created by Onur Küçük on 6.02.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import UIKit
class SignUpTableVC: UITableViewController, UITextFieldDelegate {

    var nameTextField:UITextField!
    var surnameTextField:UITextField!
    var emailTextField:UITextField!
    var phoneTextField:UITextField!
    var passwordTextField:UITextField!
    let placeHolderTexts = ["Ad","Soyad","E-posta","Telefon","Şifre"]
    let fireService = LoginFunctions()
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Backend Related
    func signUpUser(dentist:Dentist){
        fireService.signUpDentist(dentist: dentist) { (success:Bool, error:Error?) in
            if success == true {
                    print("dentist successfully created on firebase")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mainVC = storyboard.instantiateInitialViewController()
                self.present(mainVC!, animated: true, completion: nil)
        }
            else {
                print("dentist sign up failed")
            }
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeHolderTexts.count + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < placeHolderTexts.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath)
            let currentTextField = cell.viewWithTag(1) as! UITextField
            currentTextField.placeholder = placeHolderTexts[indexPath.row]
            switch indexPath.row {
            case 0:
                nameTextField = currentTextField
            case 1:
                surnameTextField = currentTextField
            case 2:
                emailTextField = currentTextField
                emailTextField.autocapitalizationType = .none
                emailTextField.keyboardType = .emailAddress
            case 3:
                phoneTextField = currentTextField
                phoneTextField.keyboardType = .phonePad
            case 4:
                passwordTextField = currentTextField
                passwordTextField.autocapitalizationType = .none
                passwordTextField.isSecureTextEntry = true
            default:
                print("")
            }
            return cell
        }
        else {
            passwordTextField.isSecureTextEntry = true
            phoneTextField.keyboardType = .phonePad
            emailTextField.keyboardType = .emailAddress
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActionCell", for: indexPath)
            (cell.viewWithTag(1) as! UILabel).text = "Kayıt Ol"
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == placeHolderTexts.count && checkSignUpConditions() {
            let dentist = Dentist()
            dentist.dentistEmail = emailTextField.text!
            dentist.dentistPassword = passwordTextField.text!
            dentist.dentistPhone = (Int)(phoneTextField.text!)
            dentist.dentistName = nameTextField.text!
            dentist.dentistSurname = surnameTextField.text!
            self.signUpUser(dentist: dentist)
        }
    }
    
    //MARK: Helper Functions
    func checkSignUpConditions() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        print("Entered email \(emailTextField.text!)")
        if emailTest.evaluate(with: emailTextField.text) == true {
            if nameTextField.text != "" && surnameTextField.text != "" && phoneTextField.text != "" && passwordTextField.text != "" {
                return true
            }
            return false
        }
        return false
    }
}
