//
//  CreatePatientVC.swift
//  iClinic
//
//  Created by Onur Küçük on 30.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

protocol PatientDelegate {
    func patientAdded()
}

class CreatePatientVC: BaseVC, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var saveButton:UIButton!
    
    private var patientName:UITextField?
    private var patientSurname:UITextField?
    private var patientPhone:UITextField?
    private var patientEmail:UITextField?
    
    var newPatient:Patient?
    var editingPatient:Patient?
    var dbPatients = [Patient]()
    var delegate:PatientDelegate?
    var isCreatingProcess = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Backend Related
    
    func createPatientToFirebase(patient:Patient){
        showActivityIndicator()
        fireService?.createPatient(patient: patient, completionFunction: { (success:Bool, error:Error?) in
            self.stopActivityIndicator()
            if error == nil {
                self.delegate?.patientAdded()
                _ = self.navigationController?.popViewController(animated: true)
            }
            else {
                self.showAlertController(withTitle: "Hata", andMessage: "Beklenmedik bir hata oluştu lütfen tekrar deneyin")
                print(error!.localizedDescription)
            }
        })
    }
    
    func updatePatient(){
        showActivityIndicator()
        self.fireService?.updatePatient(patient: editingPatient!, completionFunction: { (success:Bool, error:Error?) in
            self.stopActivityIndicator()
            if error == nil && success == true {
                let alert = UIAlertController(title: "", message: "Hasta bilgileri başarıyla güncellendi.", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (act) in
                    _ = self.navigationController?.popViewController(animated: true)
                })
                alert.addAction(alertAction)
                self.present(alert, animated: true, completion: nil)
            }
            else {
                self.showAlertController(withTitle: "Hata", andMessage: "Beklenmedik bir hata oluştu lütfen tekrar deneyin")
                print(error!.localizedDescription)
            }
        })
    }
    
    //MARK: Table View Functions
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isCreatingProcess {
            return configureCellForCreateingPatient(indexPath: indexPath)
        }
        return configureCellForEditingPatient(indexPath: indexPath)
    }
    
    func configureCellForCreateingPatient(indexPath:IndexPath) -> UITableViewCell{
        if indexPath.row < 4 {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CreatingPatientCell", for: indexPath) as! CreatingPatientTableViewCell
            switch indexPath.row {
            case 0:
                cell.configureCellWith(placeHolderText: "Adı")
                cell.textField.keyboardType = .namePhonePad
                cell.textField.autocapitalizationType = .sentences
                self.patientName = cell.textField
            case 1:
                cell.configureCellWith(placeHolderText: "Soyadı")
                cell.textField.keyboardType = .namePhonePad
                cell.textField.autocapitalizationType = .sentences
                self.patientSurname = cell.textField
            case 2:
                cell.configureCellWith(placeHolderText: "Telefon")
                cell.textField.keyboardType = .phonePad
                self.patientPhone = cell.textField
            case 3:
                cell.configureCellWith(placeHolderText: "E-Posta")
                cell.textField.keyboardType = .emailAddress
                self.patientEmail = cell.textField
            default:
                print("")
            }
            return cell
        }
        else {
            return tableView.dequeueReusableCell(withIdentifier: "CreateCell", for: indexPath)
        }
    }
    
    
    func configureCellForEditingPatient(indexPath:IndexPath) -> UITableViewCell{
        if indexPath.row < 4 {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CreatingPatientCell", for: indexPath) as! CreatingPatientTableViewCell
            switch indexPath.row {
            case 0:
                cell.configureCellForEditing(placeHolder: "Adı", value: editingPatient!.patientName)
                cell.textField.keyboardType = .namePhonePad
                cell.textField.autocapitalizationType = .sentences
                self.patientName = cell.textField
            case 1:
                cell.configureCellForEditing(placeHolder: "Soyadı", value: editingPatient!.patientSurname)
                cell.textField.keyboardType = .namePhonePad
                cell.textField.autocapitalizationType = .sentences
                self.patientSurname = cell.textField
            case 2:
                cell.configureCellForEditing(placeHolder: "Telefon", value: "\(editingPatient!.patientPhone)")
                cell.textField.keyboardType = .phonePad
                self.patientPhone = cell.textField
            case 3:
                cell.configureCellForEditing(placeHolder: "E-posta", value: editingPatient!.patientEmail)
                cell.textField.keyboardType = .emailAddress
                self.patientEmail = cell.textField
            default:
                print("")
            }
            return cell
        }
        else {
            return tableView.dequeueReusableCell(withIdentifier: "CreateCell", for: indexPath)
        }
    }
    
    
    //MARK: Helper
    
    func isThePhoneValid() -> Bool {
        return patientPhone?.text?.characters.count == 10 || patientPhone?.text?.characters.count == 11
    }
    
    func isPatientSuitibleForSaving() -> Bool {
        newPatient = Patient()
        if patientEmail?.text != "" && patientName?.text != "" && patientSurname?.text != "" && patientPhone?.text != "" && isThePhoneValid() {
            newPatient!.patientName = patientName!.text!
            newPatient!.patientSurname = patientSurname!.text!
            newPatient!.patientPhone = (Int)((patientPhone?.text)!)!
            newPatient!.patientEmail = patientEmail!.text!
            if !checkPatientIsAlreadySaved(patient: newPatient!) {
                return true
            }
            return false
        }
        return false
    }
    
    @IBAction func addPatient(_ sender: UIButton) {
        if isCreatingProcess {
            if isPatientSuitibleForSaving() {
                self.createPatientToFirebase(patient: newPatient!)
            }
            else {
                self.showAlertController(withTitle: "Hata", andMessage: "Lütfen tüm alanları eksiksiz doldurduğunuzdan emin olun")
            }
        }
        else {
            self.implementUpdate()
        }
    }
    
    func checkPatientIsAlreadySaved(patient:Patient) -> Bool{
        for dbPat in dbPatients {
            if dbPat.patientPhone == patient.patientPhone {
                return true
            }
        }
        return false
    }
    
    func implementUpdate(){
        let isThereAnyChange = patientEmail?.text != editingPatient!.patientEmail || patientName?.text != editingPatient!.patientName || patientSurname?.text != editingPatient!.patientSurname || patientPhone?.text != "\(editingPatient!.patientPhone)"
        let suitableChecking = patientEmail?.text != "" && patientName?.text != "" && patientSurname?.text != "" && patientPhone?.text != ""
        
        if isThereAnyChange && suitableChecking && isThePhoneValid() {
            editingPatient?.patientName = (patientName?.text)!
            editingPatient?.patientSurname = (patientSurname?.text)!
            editingPatient?.patientPhone = Int(patientPhone!.text!)!
            editingPatient?.patientEmail = (patientEmail?.text)!
            self.updatePatient()
        }
        else {
            self.showAlertController(withTitle: "Hata", andMessage: "Lütfen girdiğiniz bilgileri kontrol ediniz.")
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
