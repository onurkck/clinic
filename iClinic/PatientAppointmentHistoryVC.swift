//
//  PatientAppointmentHistoryVC.swift
//  iClinic
//
//  Created by Onur Küçük on 12.12.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit
protocol NoteChangedAppointmentHistDelegate {
    func noteChangeInAppointmentHist(currentAppointment:Appointment)
}
class PatientAppointmentHistoryVC: BaseVC, UITableViewDelegate, UITableViewDataSource, AppointmentHistoryDelegate {

    @IBOutlet weak var tableView:UITableView!
    var currentPatient = Patient()
    var appointmentHistory = [Appointment]()
    var delegate:NoteChangedAppointmentHistDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.createAppointmentForCurrentPatient))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Backend Related
    func updateAppointmentNote(selectedAppointment:Appointment){
        fireService?.updateAppointmentNote(appointment: selectedAppointment, completionFunction: { (success:Bool, error:Error?) in
            if error == nil && success {
                print("\nAPPOINTMENT NOTE UPDATED BOTH ON THE FIREBASE SUCCESSFULLY")
                self.delegate?.noteChangeInAppointmentHist(currentAppointment: selectedAppointment)
            }
            else {
                print("\nAPPOINTMENT COULDN'T UPDATE ON THE FIREBASE")
            }
        })
    }
    
    func deleteAppointment(appointment:Appointment){
        fireService?.deleteAnAppointment(appointment: appointment, completionFunction: { (success:Bool, error:Error?) in
            if success == true {
                self.deleteAppointmentFromDB(appointment: appointment)
                print("\nAPPOINTMENT WAS DELETED FROM FIREBASE SUCCESSFULLY.")
            }
            else {
                print("\nAPPOINTMENT COULDN'T DELETED FROM FIREBASE.")
            }
        })
    }
    
    func deleteAppointmentFromDB(appointment:Appointment){
        dbService?.deleteAppointment(appointment: appointment, completionFunction: { (dbSuccess:Bool, error:Error?) in
            if dbSuccess == true {
                print("\nAPPOINTMENT WAS DELETED FROM DB SUCCESSFULLY.")
            }
            else {
                print("\nAPPOINTMENT COULDN'T DELETED FROM DB")
            }
        })
    }
    
    //MARK: Table View Related
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if appointmentHistory.count > 0 {
            return appointmentHistory.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if appointmentHistory.count == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "NoAppointmentHistory", for: indexPath)
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentHistoryCell", for: indexPath) as! AppointmentHistoryTableViewCell
        cell.delegate = self
        cell.Appointment = appointmentHistory[indexPath.row]
        cell.configureCellWithAppointment()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if appointmentHistory.count != 0 {
            return 166.0
        }
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if appointmentHistory.count > 0 {
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if appointmentHistory.count > 0 {
            if editingStyle == UITableViewCellEditingStyle.delete {
                let willDeletedAppointment = appointmentHistory[indexPath.row]
                appointmentHistory.remove(at: indexPath.row)
                tableView.reloadData()
                deleteAppointment(appointment: willDeletedAppointment)
            }
        }
    }
    
    func createAppointmentForCurrentPatient(){
        self.performSegue(withIdentifier: "createAppointment", sender: self)
    }
    
    //MARK: Delegates
    
    func appointmentNoteWasEnteredWith(appointment: Appointment) {
        self.updateAppointmentNote(selectedAppointment: appointment)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "createAppointment" {
            let destVC = segue.destination as! CreateAppointmentVC
            destVC.selectedPatient = self.currentPatient
        }
    }
}
