//
//  CalendarVC.swift
//  iClinic
//
//  Created by Onur Küçük on 18.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit
import FSCalendar

enum ButtonState:Int {
    case CrossButtonWillBeShown = 0
    case PlusButtonWillBeShown = 1
}

class CalendarVC: BaseVC, UIPopoverPresentationControllerDelegate, UITableViewDataSource, UITableViewDelegate, AppointmentDelegate, CalendarCellDelegate, FSCalendarDataSource, FSCalendarDelegate{
    @IBOutlet weak var calendarView:FSCalendar!
    @IBOutlet weak var addButton:UIButton!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var appointmentContainerView: UIView!
    @IBOutlet weak var addPatientContainerView: UIView!
    @IBOutlet weak var myPatientsContainerView:UIView!
    @IBOutlet weak var settingsContainerView:UIView!
    private var collectionViewShouldReload:Bool!
    private var selectedDate:String!
    private var appointments = [Appointment]()
    private var buttonState = ButtonState.CrossButtonWillBeShown
    private var shouldAppointmentsCheck = true
    private let crossRotationAngle = 3*CGFloat.pi
    private let plusRotationAngle = 4*CGFloat.pi
    var filteredAppointments = [Appointment]()
    var dateArrayInString = [String]()
    var fireDates = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getAppointments()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Backend Related Functions
    func getAppointments(){
        dbService?.getAppointmentsFromDB(datesArrayInString: dateArrayInString, completionFunction: { (returnedAppointments:[Appointment]?, error:Error?) in
            if error == nil, let returned = returnedAppointments {
                self.appointments = returned
                self.checkFirebaseAppointments()
                self.filteredAppointments = self.getPatientsForDate(date: DateConversion.sharedInstance.convertDateToDBFormat(date: Date()))
                self.tableView.reloadData()
                DispatchQueue.main.async {
                    self.calendarView.reloadData()
                }
    
            }
            else {
                self.showAlertController(withTitle: "Hata", andMessage: error!.localizedDescription)
            }
        })
    }
    
    //MARK: - Calendar View Delegate & Data Source Methods
    
    func calendar(_ calendar: FSCalendar, hasEventFor date: Date) -> Bool {
        let convertedDate = DateConversion.sharedInstance.convertDateToDBFormat(date: date)
        if self.appointments.contains(where: {$0.date! == convertedDate}) {
            return true
        }
        return false
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.filteredAppointments = self.getPatientsForDate(date: DateConversion.sharedInstance.convertDateToDBFormat(date: date))
        print("Selected date is \(date)")
        self.selectedDate = DateConversion.sharedInstance.convertDateToDBFormat(date: date)
        self.tableView.reloadData()
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.getCurrentMonthDates()
        self.getAppointments()
        
    }
    
    //MARK: - Table View Related
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredAppointments.count != 0 {
            return filteredAppointments.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if filteredAppointments.count > 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentCell", for: indexPath)
        let timeLabel = cell.viewWithTag(1) as! UILabel
        let nameSurnameLabel = cell.viewWithTag(2) as! UILabel
        let tempAppointment = filteredAppointments[indexPath.row]
        timeLabel.text = "\(tempAppointment.startTime!) - \(tempAppointment.endTime!)"
        nameSurnameLabel.text = "\(tempAppointment.patient.patientName) \(tempAppointment.patient.patientSurname)"
            return cell
        }
        else {
            return tableView.dequeueReusableCell(withIdentifier: "NoAppointment", for: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDateDetail", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    //MARK: - Helper Functions
    
    func getPatientsForDate(date:String) -> [Appointment] {
        filteredAppointments = [Appointment]()
        for appointment in self.appointments {
            if date == appointment.date {
                filteredAppointments.append(appointment)
            }
        }
        return filteredAppointments
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @IBAction func addButtonTouched(_ sender: UIButton){
        
        UIView.animate(withDuration: 0.2, delay: 0, options:.curveEaseIn, animations: {
            self.addButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
            self.addButton.alpha = 0.0
            
        }) { (result) in
            if self.buttonState == ButtonState.CrossButtonWillBeShown {
                self.showBlurEffect()
            }
            else {
                self.removeBlurEffect()
            }
        }
    }
    
    
    //MARK: Delegates
    func dateSelected(from: Date, to: Date) {
        let compareResult = from.compare(to)
        if compareResult == .orderedAscending {
            collectionViewShouldReload = true
        }
        else if compareResult == .orderedDescending {
            showAlertController(withTitle: "Hata", andMessage: "Bitiş tarihi, başlangıçtan ileri bir tarih olmalıdır.")
        }
        else if compareResult == .orderedSame {
            showAlertController(withTitle: "Hata", andMessage: "Başlangıç ve bitiş tarihleri birbirinden farklı olmalıdır.")
        }
    }
    
    func appointmentCreated() {
        self.getAppointments()
    }
    func dateSelectedFromTextView(date: String) {
        self.selectedDate = date
        performSegue(withIdentifier: "showDateDetail", sender: self)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDateDetail" {
            let destVC = segue.destination as! PatientsForDateVC
            destVC.navigationItem.title = selectedDate
            destVC.selectedDate = DateConversion.sharedInstance.convertDBDateToDate(dateInString: selectedDate)
            destVC.appointments = self.filteredAppointments
        }
        else if segue.identifier == "createAppointment" {
            let destVC = segue.destination as! CreateAppointmentVC
            destVC.delegate = self
        }
        else if segue.identifier == "addPatient" {
            
        }
    }
    
    //MARK: - Helper Functions
    func showBlurEffect(){
        Animation.sharedInstance.addBlurEffectTo(views: [self.calendarView, self.tableView], withViews: [self.appointmentContainerView,self.addPatientContainerView,self.myPatientsContainerView, self.settingsContainerView])
        self.showCrossButton()
    }
    
    func removeBlurEffect(){
        Animation.sharedInstance.removeBlurEffectFrom(views: [self.calendarView, self.tableView], withViews: [self.appointmentContainerView,self.addPatientContainerView,self.myPatientsContainerView, self.settingsContainerView])
        self.showPlusButton()
    }
    
    func showPlusButton(){
        self.addButton.setImage(#imageLiteral(resourceName: "plus"), for: .normal)
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.addButton.transform = CGAffineTransform(rotationAngle: self.plusRotationAngle)
            self.addButton.alpha = 1.0
            
        }) { (result) in
            self.buttonState = .CrossButtonWillBeShown
        }
        
    }
    
    func showCrossButton(){
        self.addButton.setImage(#imageLiteral(resourceName: "cross"), for: .normal)
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.addButton.transform = CGAffineTransform(rotationAngle: self.crossRotationAngle)
            self.addButton.alpha = 1.0
            
        }) { (result) in
            self.buttonState = .PlusButtonWillBeShown
        }
    }

    
    func checkFirebaseAppointments() {
        if shouldAppointmentsCheck {
            self.shouldAppointmentsCheck = false
            SynchronisationStuff.sharedInstance.checkAppointments(dbAppointments: self.appointments, datesArrayInString: self.dateArrayInString)
        }
    }
    
    func prepareUI(){
        collectionViewShouldReload = false
        getCurrentMonthDates()
        selectedDate = DateConversion.sharedInstance.convertDateToDBFormat(date: Date())
        let tapGestureForAppointment = UITapGestureRecognizer(target: self, action: #selector(self.showAddAppointment))
        let tapGestureForMyPatients = UITapGestureRecognizer(target: self, action: #selector(self.showMyPatients))
        let tapGestureForAddingPatients = UITapGestureRecognizer(target: self, action: #selector(self.showAddPatient))
        let tapGestureForSettings = UITapGestureRecognizer(target: self, action: #selector(self.showSettingsScreen))
        
        configureTapGestureFor(containerView:appointmentContainerView, tapGesture: tapGestureForAppointment)
        configureTapGestureFor(containerView: myPatientsContainerView, tapGesture: tapGestureForMyPatients)
        configureTapGestureFor(containerView: addPatientContainerView, tapGesture: tapGestureForAddingPatients)
        configureTapGestureFor(containerView: settingsContainerView, tapGesture: tapGestureForSettings)
    }
    
    func getCurrentMonthDates() {
        self.fireDates.removeAll()
        self.dateArrayInString.removeAll()
        for i in 0...42 {
            let date = Calendar.current.date(byAdding: .day, value: i, to: calendarView.currentPage)
            self.dateArrayInString.append(DateConversion.sharedInstance.convertDateToDBFormat(date: date!))
            self.fireDates.append(DateConversion.sharedInstance.convertDateToFireNodeFormat(date: date!))
        }
    }
    
    func configureTapGestureFor(containerView:UIView, tapGesture:UITapGestureRecognizer) {
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        containerView.addGestureRecognizer(tapGesture)
    }
    
    func showAddPatient(){
        print("Add patient screen will be shown")
        self.performSegue(withIdentifier: "addPatient", sender: self)
        self.removeBlurEffect()
    }
    func showAddAppointment(){
        print("Create appointment touched")
        self.performSegue(withIdentifier: "createAppointment", sender: self)
        self.removeBlurEffect()
    }
    func showMyPatients(){
        print("My patients will be shown")
        self.performSegue(withIdentifier: "showMyPatients", sender: self)
        self.removeBlurEffect()
    }
    func showSettingsScreen(){
        print("My patients will be shown")
        self.performSegue(withIdentifier: "showSettings", sender: self)
        self.removeBlurEffect()
    }
}
