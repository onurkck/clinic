//
//  SynchronisationStuff.swift
//  iClinic
//
//  Created by Onur Küçük on 8.05.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import UIKit

class SynchronisationStuff: NSObject {

    var firebaseService:FirebaseService = FirebaseImplementation()
    var dbService:DBServices = DBServicesImplementation()
    
    static let sharedInstance = SynchronisationStuff()
    
    func checkAppointments(dbAppointments:[Appointment], datesArrayInString:[String]) {
        let start = DispatchTime.now()
        firebaseService.getDentistAppointmentForDateInterval(dentist: currentDentist, datesArrayInString: datesArrayInString) { (returnedAppointments:[Appointment]?, error:Error?) in
            if error == nil, let appointments = returnedAppointments{
                let end = DispatchTime.now()
                let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
                let timeInterval = Double(nanoTime) / 1_000_000_000
                print("It takes \(timeInterval) seconds")
                print("The number of fire appointments : \(appointments.count)")
                self.checkForDeletingAppointments(dbAppointments: dbAppointments, fireAppointments: appointments)
                for appointment in appointments {
                    if dbAppointments.contains(where: {$0.appointmentID == appointment.appointmentID}){
                        print("Appointment exists in db")
                    }
                    else {
                        self.dbService.insertDatas(dbName: .AppointmentDatabase, datas: appointment.configureAppointmentForDB(), completionFunction: { (success:Bool, dbError:Error?) in
                            if dbError == nil && success {
                                print("Firebase appointment added to DB as a result of sync")
                                LocalNotification.shared.setNotificationFor(appointment: appointment)
                            }
                            else {
                                print("Firebase appointment couldn't be added")
                            }
                        })
                    }
                }
            }
        }
    }
    
    func checkPatients(dbPatients:[Patient]){
        firebaseService.getDentistPatients(dentist: currentDentist) { (returnedPatients:[Patient]?, error:Error?) in
            if error == nil, let patients = returnedPatients {
                self.checkForDeletingPatients(dbPatients: dbPatients, firePatient: patients)
                for patient in patients {
                    if !dbPatients.contains(where: {$0.patientID == patient.patientID}) {
                        self.dbService.insertDatas(dbName: .PatientDatabase, datas: patient.configurePatientForDB(), completionFunction: { (success:Bool, dbError:Error?) in
                            if dbError == nil && success {
                                print("Firebase patient added to DB as a result of sync")
                            }
                            else {
                                print("Firebase patient couldn't be added")
                            }
                        })
                    }
                }
            }
        }
    }
    
    func checkFinancialHistories(dbFinancialHistories:[FinancialHistory], patient:Patient){
        firebaseService.getFinancialHistoryOf(patient: patient) { (returnedFinancialHistory:[FinancialHistory]?, error:Error?) in
            if error == nil, let financialHistories = returnedFinancialHistory {
                for financialHist in financialHistories {
                    if !dbFinancialHistories.contains(where: {$0.financialHistoryID == financialHist.financialHistoryID}) {
                        self.dbService.insertDatas(dbName: .FinancialHistDatabase, datas: financialHist.configureFinancialHistForDB(), completionFunction: { (success:Bool, dbError:Error?) in
                            if dbError == nil && success {
                                print("Firebase patient added to DB as a result of sync")
                            }
                            else {
                                print("Firebase appointment couldn't be added")
                            }
                        })
                    }
                }
            }
        }
    }
    
    //MARK: Helper
    
    func checkForDeletingAppointments(dbAppointments:[Appointment], fireAppointments:[Appointment]) {
        for appointment in dbAppointments {
            if fireAppointments.contains(where: {$0.appointmentID == appointment.appointmentID}){
                print("Fire appointments contains this appointment")
            }
            else {
                self.dbService.deleteAppointment(appointment: appointment, completionFunction: { (success:Bool, error:Error?) in
                    if error == nil && success {
                        print("Appointment successfully removed from db")
                        LocalNotification.shared.deleteNotificationOf(appointment: appointment)
                    }
                    else {
                        print("Appointment couldn't remove from db")
                    }
                })
            }
        }
    }
    
    func checkForDeletingPatients(dbPatients:[Patient], firePatient:[Patient]) {
        for patient in dbPatients {
            if !firePatient.contains(where: {$0.patientID == patient.patientID}) {
                self.dbService.deletePatient(patient: patient, completionFunction: { (success:Bool, error:Error?) in
                    if error == nil && success {
                        print("Patient successfully removed from db")
                    }
                    else {
                        print("Patient couldn't remove from db")
                    }
                })
            }
        }
    }
}
