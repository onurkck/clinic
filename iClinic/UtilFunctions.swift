//
//  UtilFunctions.swift
//  iClinic
//
//  Created by Onur Küçük on 8.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration

var currentDentist:Dentist!
class UtilFunctions{

    static let sharedInstance = UtilFunctions()
    var activityIndicator:UIActivityIndicatorView!
    var loaderView:UIView!

    //MARK: - UI Related
    
    func showAlertController(withTitle:String, andMessage:String) -> UIAlertController {
        let alertController = UIAlertController(title: withTitle, message: andMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alertController
    }
    
    func showLoader(viewController:UIViewController){
        viewController.view.isUserInteractionEnabled = false
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        loaderView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        loaderView.alpha = 0.6
        loaderView.center = viewController.view.center
        loaderView.backgroundColor = UIColor.black
        loaderView.layer.cornerRadius = 10
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        loaderView.addSubview(activityIndicator)
        viewController.view.addSubview(loaderView)
    }
    
    //MARK: - Helpers
    
    func sortDBPatientArray(arrayForSorting:[Patient]) -> [Patient]{
        return arrayForSorting.sorted(by: {$0.patientName < $1.patientName})
    }
    
    func sortPatientsWithTime(arrayForSorting:[Appointment]) -> [Appointment] {
        return arrayForSorting.sorted(by: {$0.startTime! < $1.startTime!})
    }
    
    func stopActivityIndicator(viewController:UIViewController){
        viewController.view.isUserInteractionEnabled = true
        loaderView.removeFromSuperview()
        activityIndicator.stopAnimating()
    }



    func isTheDeviceiPad() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    //should financial hist create?
    func shouldCreateFinancialHistObjectFor(finance:Finance, enteredTotalCost:Int, enteredPaidAmount:Int, enteredNotes:String?) -> (shoulCreateFinancialHist:Bool, finance:Finance) {
        //TODO: Think about troubles (entered>total amount, New entered amount or finished finance)
        var result = false
        let anyPaymentEntered = (enteredPaidAmount != 0 && (enteredPaidAmount < finance.totalCost || enteredPaidAmount < enteredTotalCost))
        let isNewTotalAmountEntered = finance.totalCost != enteredTotalCost
        let isTheNoteChanged = finance.financeNotes != enteredNotes
        
        if anyPaymentEntered || isNewTotalAmountEntered || isTheNoteChanged {
            if isTheNoteChanged {
                finance.financeNotes = enteredNotes!
            }
            if isNewTotalAmountEntered && anyPaymentEntered {
                finance.balance = finance.balance + (enteredTotalCost - finance.totalCost) - enteredPaidAmount
                finance.paidAmount = enteredPaidAmount
                finance.totalCost = enteredTotalCost
                finance.lastPaymentDate = Date()
                result = true
            }
            if anyPaymentEntered && !isNewTotalAmountEntered {
                finance.balance = finance.balance - enteredPaidAmount
                finance.paidAmount = enteredPaidAmount
                finance.lastPaymentDate =  Date()
                result = true
            }
            if isNewTotalAmountEntered && !anyPaymentEntered {
                finance.balance = finance.balance + (enteredTotalCost -  finance.totalCost)
                finance.totalCost = enteredTotalCost
            }
        }
        return (result, finance)
    }
}

class ApplicationAttributes {
    static let blueColor = UIColor(colorLiteralRed: 104/255, green: 207/255, blue: 235/255, alpha: 1)
    static let lightPink = UIColor(colorLiteralRed: 250/255, green: 233/255, blue: 253/255, alpha: 1)
    static let darkPink = UIColor(colorLiteralRed: 255/255, green: 150/255, blue: 237/255, alpha: 1)
}
