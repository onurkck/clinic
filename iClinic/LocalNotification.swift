//
//  LocalNotification.swift
//  iClinic
//
//  Created by Onur Küçük on 25.05.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import UIKit

class LocalNotification: NSObject {
    static let shared = LocalNotification()
   
    func setNotificationFor(appointment:Appointment) {
        let notification = UILocalNotification()
        let appointmentDate = appointment.startDateTime
        let notificationDate = Calendar.current.date(byAdding: .minute, value: -10, to: appointmentDate)
        
//        notification.alertBody = "\(DateConversion.sharedInstance.convertTimeToDBFormat(date: appointment.startDateTime)) - \(appointment.patient.patientName) \(appointment.patient.patientSurname)"
        notification.alertTitle = "Gelecek Hasta"
        notification.fireDate = notificationDate
        notification.userInfo = ["uuid":"\(appointment.appointmentID)"]
        notification.soundName = UILocalNotificationDefaultSoundName
        if notificationDate! > Date() {
        UIApplication.shared.scheduleLocalNotification(notification)
            print("Notification was set")
        }
        else {
            print("Notification timer expired")
        }
    }
    
    func deleteNotificationOf(appointment:Appointment) {
        for notf in UIApplication.shared.scheduledLocalNotifications! {
            if notf.userInfo?["uuid"] as! String == appointment.appointmentID {
                UIApplication.shared.cancelLocalNotification(notf)
                print("Notification was deleted")
            }
        }
    }
}
