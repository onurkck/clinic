//
//  MainVC.swift
//  iClinic
//
//  Created by Onur Küçük on 9.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class MainVC: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    var sectionArray = [String]()
    var patientsForDate = [Appointment]()
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Backend Related Functions
    
//    func getUpcomingAppointments(){
//        let aWeekLater = Date().addingTimeInterval(604800)
//        self.showActivityIndicator
//        
//        fireService?.getApppointmentsForDateInterval(dentist: currentDentist, from: Date(), to: aWeekLater, completionFunction: { (returnedAppointments:[Appointment]?, error:Error?) in
//            self.stopActivityIndicator()
//            if error == nil {
//                self.upcomingAppointments = returnedAppointments!
//                self.constructSectionsAndReloadTableView()
//            }
//            else{
//                self.showAlertController(withTitle: "Hata", andMessage: (error?.localizedDescription)!)
//            }
//        })
//    }
    
    //MARK: - Table View Related Functions
    
    func numberOfSections(in tableView: UITableView) -> Int {
//        if upcomingAppointments.count > 0 {
//            return sectionArray.count
//        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if upcomingAppointments.count > 0 {
//            return self.appointmentsFor(sectionString: sectionArray[section]).count
//        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if upcomingAppointments.count == 0 {
//            return tableView.dequeueReusableCell(withIdentifier: "NoAppointment", for: indexPath)
//        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientCell", for: indexPath) as! UpcomingAppointmentCell
        
//        _ = self.appointmentsFor(sectionString: sectionArray[indexPath.section])
//        cell.configureAppointmentCellWith(appointment: appointmentArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if upcomingAppointments.count > 0 {
//            return sectionArray[section]
//        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
////        if upcomingAppointments.count > 0{
////            return 224.0
////        }
//        else {
            return 44.0
//        }
    }
    
    //MARK: - Util Functions
    //    func constructSections(){
    //        let today = Date()
    //        let aDayInSeconds = 86400
    //        let dateFormatter = DateFormatter()
    //        dateFormatter.dateFormat = "dd/MM/yyyy"
    //        for i in 0...6{
    //            let constructedDay = today.addingTimeInterval(TimeInterval((Int)(aDayInSeconds * i)))
    //            let dayInString = dateFormatter.string(from: constructedDay)
    //            sectionArray.append(dayInString)
    //        }
    //    }
    func constructSectionsAndReloadTableView(){
//        for appointment in upcomingAppointments {
//            let appointmentDateInString = appointment.appointment.date
//            if !sectionArray.contains(appointmentDateInString!) {
//                sectionArray.append(appointmentDateInString!)
//            }
//        }
//        self.tableView.reloadData()
    }
    
//    func appointmentsFor(sectionString:String) -> [Appointment]{
//        var tempAppointments = [Appointment]()
//        for appointment in upcomingAppointments{
//            if sectionString == dateFormatter.string(from: appointment.dateTime) {
//                tempAppointments.append(appointment)
//            }
//        }
//        return tempAppointments
//    }
    
    // MARK: - Navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//    }

}
