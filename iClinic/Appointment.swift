//
//  Appointment.swift
//  iClinic
//
//  Created by Onur Küçük on 29.10.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit
import FMDB
class Appointment: NSObject {

    var appointmentID = ""
    var patient:Patient!
    var patientID = ""
    var appointmentNotes = ""
    var startDateTime = Date()
    var endDateTime = Date()
    
    var date:String?
    var startTime:String?
    var endTime:String?
    

    static func initAppointmentWith(dictionary:NSDictionary) -> Appointment {
        let appointment = Appointment()
        let dbService:DBServices = DBServicesImplementation()
        appointment.appointmentID = dictionary.value(forKey: "appointmentID") as! String
        appointment.appointmentNotes = dictionary.value(forKey: "appointmentNotes") as! String
        appointment.startDateTime = (dictionary.value(forKey: "appointmentStartDate") as! String).convertToDate
        appointment.endDateTime = (dictionary.value(forKey: "appointmentEndDate") as! String).convertToDate
        appointment.patientID = dictionary.value(forKey: "patient") as! String
        //TODO: If the patient doesn't exist in the db, there will be crash
        dbService.getPatientWith(patientID: appointment.appointmentID) { (returnedPatient:Patient?, error:Error?) in
            if error == nil, let patient = returnedPatient {
                appointment.patient = patient
            }
        }
        return appointment
    }
    
    static func initAppointmentResultSet(resultSet:FMResultSet) -> Appointment {
        let appointment = Appointment()
        let dbService:DBServices = DBServicesImplementation()
        appointment.appointmentID = resultSet.string(forColumn: "ID")
        appointment.patientID = resultSet.string(forColumn: "PATIENT_ID")
        appointment.date = resultSet.string(forColumn: "DATE")
        appointment.appointmentNotes = resultSet.string(forColumn: "APPOINTMENT_NOTES")
        appointment.startTime = resultSet.string(forColumn: "START_TIME")
        appointment.endTime = resultSet.string(forColumn: "END_TIME")
        dbService.getPatientWith(patientID: resultSet.string(forColumn: "PATIENT_ID"), completionFunction: { (returned:Patient?, error:Error?) in
            if error == nil {
                appointment.patient = returned!
            }
        })
        return appointment
    }

    
     func configureAppointmentForDB() -> [String] {
        var stringArray = [String]()
        stringArray.append("\(self.appointmentID)")
        stringArray.append("\(self.patientID)")
        stringArray.append("\(self.appointmentNotes)")
        stringArray.append("\(DateConversion.sharedInstance.convertDateToDBFormat(date: startDateTime as Date))")
        stringArray.append("\(DateConversion.sharedInstance.convertTimeToDBFormat(date: self.startDateTime as Date))")
        stringArray.append("\(DateConversion.sharedInstance.convertTimeToDBFormat(date: self.endDateTime as Date))")
        return stringArray
    }

    func convertToDictionary() -> [String : Any] {
        return ["appointmentID":self.appointmentID, "patient":self.patientID,"appointmentStartDate":self.startDateTime.toString, "appointmentEndDate":self.endDateTime.toString, "appointmentNotes":self.appointmentNotes] as [String : Any]
    }
}
