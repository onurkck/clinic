//
//  PatientVC.swift
//  iClinic
//
//  Created by Onur Küçük on 11.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

protocol PatientSelectionDelegate {
    func patientSelected(selectedPatient:Patient)
}

class PatientVC: BaseVC, UITableViewDataSource,UITableViewDelegate,PatientDelegate, UISearchBarDelegate, UISearchDisplayDelegate {
    
    var patientArray:[Patient]!
    var delegate:PatientSelectionDelegate?
    var isCreatingAppointment = false
    var selectedPatient:Patient?
    var patientsSearchResults:[Patient]?
    var selectedPatientFinancialStatus:Finance!
    
    @IBOutlet weak var tableView:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Hastalarım"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(self.addPatient))
        self.getPatientsFromDB()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - DB Related Functions
    
    func getPatientsFromDB(){
        dbService?.getAllPatientsFromDB(completionFunction: { (returnedPatients:[Patient]?, error:Error?) in
            if error == nil, let patients = returnedPatients {
                self.patientArray = patients
                self.tableView.reloadData()
                self.checkFirePatientsAndReloadViewIfNeeded()
            }
        })
    }
    
    //MARK: - Table View Related Functions
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return patientsSearchResults?.count ?? 0
        }
        
        if let patients = patientArray{
            return patients.count
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return configurePatientCellWithDBPatient(patients: patientsSearchResults!, indexPath: indexPath)
            
        }
        
        if patientArray != nil{
            return configurePatientCellWithPatientObj(patients: patientArray, indexPath: indexPath)
        }
        else {
            return tableView.dequeueReusableCell(withIdentifier: "NoPatient", for: indexPath)
        }
    }
    
    func configurePatientCellWithPatientObj(patients:[Patient], indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientCell", for: indexPath) //Revise id
        let patientNameSurnameLabel = cell.viewWithTag(1) as! UILabel
        let patient = patients[indexPath.row]
        patientNameSurnameLabel.text = "\(patient.patientName) \(patient.patientSurname)"
        
        if isCreatingAppointment {
            cell.accessoryType = .none
        }
        return cell
    }
    
    func configurePatientCellWithDBPatient(patients:[Patient], indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientCell", for: indexPath) //Revise id
        let patientNameSurnameLabel = cell.viewWithTag(1) as! UILabel
        let patient = patients[indexPath.row]
        patientNameSurnameLabel.text = "\(patient.patientName) \(patient.patientSurname)"
        if isCreatingAppointment {
            if let selected = self.selectedPatient {
                if selected.patientID == patient.patientID {
                    cell.accessoryType = .checkmark
                }
                else {
                    cell.accessoryType = .none
                }
            }
            else {
                cell.accessoryType = .none
            }
        }
        else {
            cell.accessoryType = .disclosureIndicator
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if tableView == self.searchDisplayController?.searchResultsTableView {
            self.selectedPatient = patientsSearchResults?[indexPath.row]
        }
        else {
            self.selectedPatient = patientArray?[indexPath.row]
        }
        
        if isCreatingAppointment{
            tableView.dequeueReusableCell(withIdentifier: "PatientCell", for: indexPath).accessoryType = .checkmark
            self.delegate?.patientSelected(selectedPatient: self.selectedPatient!)
            _ = self.navigationController?.popViewController(animated: true)
            return
        }
        self.performSegue(withIdentifier: "showPatientDetail", sender: self)
    }
    
    func addPatient(){
        self.performSegue(withIdentifier: "addPatient", sender: self)
    }
    
    func patientAdded() {
        self.getPatientsFromDB()
    }
    
    //MARK: - Search Related
    
    func filterPatientsFor(searchText:String) {
        if self.patientArray == nil {
            self.patientsSearchResults = nil
            return
        }
        self.patientsSearchResults = self.patientArray?.filter({ (aPatient:Patient) -> Bool in
            return aPatient.patientName.lowercased().range(of: searchText.lowercased()) != nil
        })
    }
    
    func searchDisplayController(_ controller: UISearchDisplayController, shouldReloadTableForSearch searchString: String?) -> Bool {
        self.filterPatientsFor(searchText: searchString!)
        return true
    }
    
    func searchDisplayController(_ controller: UISearchDisplayController, didLoadSearchResultsTableView tableView: UITableView) {
        tableView.backgroundColor = UIColor(colorLiteralRed: 249/255.0, green: 233/255.0, blue: 255/255.0, alpha: 1)
    }
    
    //MARK: Helper Functions
    
    func checkFirePatientsAndReloadViewIfNeeded(){
        DispatchQueue.global().async {
            SynchronisationStuff.sharedInstance.checkPatients(dbPatients: self.patientArray)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addPatient"{
            let destVC = segue.destination as! CreatePatientVC
            if let patientArray = patientArray {
                destVC.dbPatients = patientArray
            }
            destVC.delegate = self
        }
        else if segue.identifier == "showPatientDetail" {
            let destVC = segue.destination as! AppointmentDetailVC
            dbService?.getPatientFinanceStatus(patient: self.selectedPatient!, completionFunction: { (returnedFinance:Finance?, error:Error?) in
                if error == nil {
                    destVC.patientFinancialStatus = returnedFinance!
                }
            })
            
            destVC.patientObject = self.selectedPatient!
        }
    }
}
