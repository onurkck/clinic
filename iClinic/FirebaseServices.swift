//
//  FirebaseServices.swift
//  iClinic
//
//  Created by Onur Küçük on 18.04.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import Foundation
import UIKit

typealias DentistReturnFunction = (Dentist?, Error?) -> Void
typealias AppointmentReturnFunction = (Appointment?, Error?) -> Void
typealias AppointmentArrayReturnFunction = ([Appointment]?, Error?) -> Void
typealias PatientReturnFunction = (Patient?, Error?) -> Void
typealias PatientArrayReturnFunction = ([Patient]?, Error?) -> Void
typealias BoolReturnFunction = (Bool, Error?) -> Void
typealias FinanceArrayReturnFunction = ([Finance]?, Error?) -> Void
typealias FinanceReturnFunction = (Finance?, Error?) -> Void
typealias PatientPhotoArrayReturnFunction = ([PatientPhoto]?, Error?) -> Void
typealias ImageReturnFunction = (UIImage?, Error?) -> Void
typealias FinancialHistoryObjectArrayReturnFunction = ([FinancialHistory]?, Error?) -> Void


protocol FirebaseService {
    
    //MARK: Patient Related Functions
    
    func createPatient(patient:Patient, completionFunction:@escaping BoolReturnFunction)
    
    func getPatientWith(id:String, completionFunction:@escaping PatientReturnFunction)
    
    func updatePatient(patient:Patient, completionFunction:@escaping BoolReturnFunction)
    
    func deletePatient(patient:Patient, completionFunction:@escaping BoolReturnFunction)
    
    func getDentistPatients(dentist:Dentist, completionFunction:@escaping PatientArrayReturnFunction)
    
    func getPatientPhotos(patient:Patient, isRadiography:Bool, completionFunction:@escaping PatientPhotoArrayReturnFunction)
    
    func uploadPatientPhoto(patient:Patient, image:UIImage, isRadiography:Bool, completionFunction:@escaping BoolReturnFunction)
    
    func download(patientPhoto:PatientPhoto, completionFunction:@escaping ImageReturnFunction)
    
    //MARK: Appointment Related Functions
    
    func createAppointment(appointment:Appointment, completionFunction:@escaping BoolReturnFunction)
    
    func updateAppointmentNote(appointment:Appointment, completionFunction:@escaping BoolReturnFunction)
    
    func deleteAnAppointment(appointment:Appointment, completionFunction:@escaping BoolReturnFunction)
    
    func getPatientAppointments(patient:Patient, completionFunction:@escaping AppointmentArrayReturnFunction)
    
    func getDentistAppointmentForDateInterval(dentist:Dentist, datesArrayInString:[String], completionFunction:@escaping AppointmentArrayReturnFunction)
    
    func getAppointmentsForTheFirstLaunch(dentist:Dentist, completionFunction:@escaping AppointmentArrayReturnFunction)
    
    
    //MARK: Finance Related Functions
    
    func getPatientFinancialStatus(patient:Patient, completionFunction:@escaping FinanceReturnFunction)
    
    func updateFinancialStatusOf(patient:Patient, finance:Finance, completionFunction:@escaping BoolReturnFunction)
    
    func getFinancialHistoryOf(patient:Patient, completionFunction:@escaping FinancialHistoryObjectArrayReturnFunction)
        
    func createFinancialHistoryForPayment(financialHist:FinancialHistory, completionFunction:@escaping BoolReturnFunction)
    
}
