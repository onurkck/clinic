//
//  Statics.swift
//  iClinic
//
//  Created by Onur Küçük on 5.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import Foundation

struct Column {
    let name:String
    let type:String
}

enum ParseClassName:String {
    case User = "_User"
    case Appointment = "Appointment"
    case AppointmentHistory = "AppointmentHistory"
    case Clinic = "Clinic"
    case Dentist = "Dentist"
    case Dentition = "Dentition"
    case FinancialHistory = "FinancialHistory"
    case PatientFinancials = "PatientFinancialHistory"
    case PatientPhotos = "PatientPhotos"
    case Patient = "Patient"
    case Specialists = "Specialists" // It could be unnecessary
    case Treatment = "Treatment"
}

class Statics {
    
    //MARK: - DB Related Constants
    
    static let patientColumnsAndTypes:[Column] = [Column(name:"ID", type:"TEXT"),
                                                  Column(name:"NAME", type:"TEXT"),
                                                  Column(name:"SURNAME", type:"TEXT"),
                                                  Column(name:"PHONE", type:"NUMBER"),
                                                  Column(name:"EMAIL", type:"TEXT"),
                                                  Column(name:"FINANCE_ID", type:"TEXT")]
    
    static let appointmentColumnsAndTypes:[Column] = [Column(name:"ID", type:"TEXT"),
                                                      Column(name:"PATIENT_ID", type:"TEXT"),
                                                      Column(name: "APPOINTMENT_NOTES", type: "TEXT"),
                                                      Column(name:"DATE", type:"TEXT"),
                                                      Column(name: "START_TIME", type: "TEXT"),
                                                      Column(name: "END_TIME", type: "TEXT")]
    
    static let financeColumnsAndTypes:[Column] = [Column(name:"ID", type:"TEXT"),
                                                  Column(name:"TOTAL_AMOUNT", type:"NUMBER"),
                                                  Column(name:"PAID_AMOUNT", type:"NUMBER"),
                                                  Column(name:"BALANCE", type:"NUMBER"),
                                                  Column(name: "IS_THERE_ANY_PAYMENT", type: "NUMBER"),
                                                  Column(name:"FINANCE_NOTES", type:"TEXT"),
                                                  Column(name:"LAST_PAYMENT_DATE", type:"TEXT")]
    
    static let financialHistoryColumnsAndTypes:[Column] = [Column(name:"ID",type:"TEXT"),
                                                           Column(name:"PATIENT_ID", type:"TEXT"),
                                                  Column(name:"PAID_AMOUNT", type:"NUMBER"),
                                                  Column(name:"PAYMENT_DATE", type:"TEXT")]
    
    static let treatmentColumnsAndTypes:[Column] = [Column(name:"TREATMENT_NAME", type:"TEXT")]
    
    
}
