//
//  DateSelectionTableVC.swift
//  iClinic
//
//  Created by Onur Küçük on 23.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

protocol DateSelectionProtocol {
    func dateSelected(from:Date, to:Date)
}


class DateSelectionTableVC: UITableViewController {

    var startDate:Date!
    var endDate:Date!
    var currentStartDate:String?
    var currentEndDate:String?
    var startDatePickerWillBeShown:Bool!
    var endDatePickerWillBeShown:Bool!
    
    var delegate:DateSelectionProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startDatePickerWillBeShown = false
        endDatePickerWillBeShown = false

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.dateSelected))

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if startDatePickerWillBeShown == true || endDatePickerWillBeShown == true {
            return 3
        }
        return 2
    }
    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let dateCell = tableView.dequeueReusableCell(withIdentifier: "DateCell", for: indexPath) as! DateInfoTableViewCell
            dateCell.dateTypeLabel.text = "Başlangıç"
            dateCell.selectedDate.text = "\(DateConversion.sharedInstance.convertDateToDBFormat(date: startDate!))"
            tableView.rowHeight = 44
            return dateCell
        }
            
        else if indexPath.row == 1 {
            if startDatePickerWillBeShown == true {
                let datePickerCell = tableView.dequeueReusableCell(withIdentifier: "DatePickerCell", for: indexPath) as! DatePickerTableViewCell
                datePickerCell.datePicker.tag = 1
                datePickerCell.datePicker.date = startDate
                tableView.rowHeight = 222
                startDatePickerWillBeShown = false
                return datePickerCell
            }else {
                let dateCell = tableView.dequeueReusableCell(withIdentifier: "DateCell", for: indexPath) as! DateInfoTableViewCell
                dateCell.dateTypeLabel.text = "Bitiş"
                dateCell.selectedDate.text = "\(DateConversion.sharedInstance.convertDateToDBFormat(date: endDate!))"
                tableView.rowHeight = 44
                return dateCell
            }
        }
            
        else {
            if endDatePickerWillBeShown == true {
                let datePickerCell = tableView.dequeueReusableCell(withIdentifier: "DatePickerCell", for: indexPath) as! DatePickerTableViewCell
                datePickerCell.datePicker.tag = 2
                datePickerCell.datePicker.date = endDate
                endDatePickerWillBeShown = false
                tableView.rowHeight = 222
                return datePickerCell
            }
            else {
                let dateCell = tableView.dequeueReusableCell(withIdentifier: "DateCell", for: indexPath) as! DateInfoTableViewCell
                dateCell.dateTypeLabel.text = "Bitiş"
                dateCell.selectedDate.text = "\(DateConversion.sharedInstance.convertDateToDBFormat(date: endDate!))"
                tableView.rowHeight = 44
                return dateCell
                
            }
        }
     }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            startDatePickerWillBeShown = true
            tableView.reloadData()
        }
        else if indexPath.row == 1 && startDatePickerWillBeShown == false {
            endDatePickerWillBeShown = true
            tableView.reloadData()
        }
        else if indexPath.row == 2 {
            endDatePickerWillBeShown = true
            tableView.reloadData()
        }
    }
    
    @IBAction func datePickerChanged(_ sender: UIDatePicker) {
        
        if sender.tag == 1 {
            startDate = sender.date
            print("Start Date = \(startDate!)")
        }
        else {
            endDate = sender.date
            print("End Date = \(endDate!)")
        }
    }
    
    func dateSelected(){
        self.dismiss(animated: true, completion: nil)
        delegate?.dateSelected(from: startDate!, to: endDate!)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
