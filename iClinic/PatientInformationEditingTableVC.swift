//
//  PatientInformationEditingTableVC.swift
//  iClinic
//
//  Created by Onur Küçük on 3.03.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import UIKit

class PatientInformationEditingTableVC: UITableViewController, UITextViewDelegate {

    var currentPatient = Patient()
    var patientInfoArray:[String]!
    
    private var patientName:UITextView?
    private var patientSurname:UITextView?
    private var patientPhone:UITextView?
    private var patientEmail:UITextView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Kaydet", style: .plain, target: self, action: #selector(self.savePatient))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Backend Related
    // Fetch parse object
    
    func savePatientChanging(){
        //Update DB & B4A
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditCell", for: indexPath) as! EditingPatientTableViewCell
        switch indexPath.row {
        case 0:
            cell.textView.text = self.currentPatient.patientName
            cell.textView.keyboardType = .namePhonePad
            self.patientName = cell.textView
        case 1:
            cell.textView.text = self.currentPatient.patientSurname
            cell.textView.keyboardType = .namePhonePad
            self.patientSurname = cell.textView
        case 2:
            cell.textView.text = "\(self.currentPatient.patientPhone)"
            cell.textView.keyboardType = .phonePad
            self.patientPhone = cell.textView
        case 3:
            cell.textView.text = self.currentPatient.patientEmail
            cell.textView.keyboardType = .emailAddress
            self.patientEmail = cell.textView
        default:
            print("Default statement")
        }
        return cell
    }
    

    func savePatient(){
        let isThereAnyChange = patientEmail?.text != currentPatient.patientEmail || patientName?.text != currentPatient.patientName || patientSurname?.text != currentPatient.patientSurname || patientPhone?.text != "\(currentPatient.patientPhone)"
        let suitableChecking = patientEmail?.text != "" && patientName?.text != "" && patientSurname?.text != "" && patientPhone?.text != ""
        
        if isThereAnyChange && suitableChecking {
            
        }
    }
}
