//
//  DateConversion.swift
//  iClinic
//
//  Created by Onur Küçük on 19.05.2017.
//  Copyright © 2017 Onur Küçük. All rights reserved.
//

import UIKit

class DateConversion: NSObject {

    static let sharedInstance = DateConversion()
    
    func convertDBDateToDate(dateInString:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.date(from: dateInString)!
    }
    
    func convertDateToDBFormat(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: date)
    }
    
    func convertDateToCellFormat(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        return dateFormatter.string(from: date)
    }
    
    func convertTimeToDBFormat(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: date)
    }
    
    func convertDateToFireNodeFormat(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "ddMMyyy"
        return dateFormatter.string(from: date)
    }
    
    func convertDBDateToFireDateFormat(date:String) -> String {
        let tempDate = self.convertDBDateToDate(dateInString: date)
        let fireDate = self.convertDateToFireNodeFormat(date: tempDate)
        return fireDate
    }
}
