//
//  AppointmentDetailVC.swift
//  iClinic
//
//  Created by Onur Küçük on 9.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

protocol NoteChangedAppointmentDetailDelegate {
    func noteChanged()
}

class AppointmentDetailVC: BaseVC, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate, NoteChangedAppointmentHistDelegate {
    @IBOutlet weak var tableView:UITableView!
    var patientFinancialStatus:Finance!
    var appointmentWithPatient:Appointment!
    var patientObject:Patient!
    var patientPhotos:[PatientPhoto]?
    var delegate:NoteChangedAppointmentDetailDelegate?
    var doesItComeFromPatientDetail = false
    var isRadiography = true
    var financialNoteTextView:UITextView!
    private var totalAmountTextfield:UITextField!
    private var paidAmountTextfield:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = patientObject.patientName
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: #imageLiteral(resourceName: "CallBarButton").size.width, height: #imageLiteral(resourceName: "CallBarButton").size.height))
        button.addTarget(self, action: #selector(callPatient), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "CallBarButton"), for: .normal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    override func viewWillAppear(_ animated: Bool) {
        if patientObject == nil {
            self.getPatientPhotoURLS()
        }
    }

    //MARK: Backend Related
    
    func getPatientPhotoURLS(){
        fireService?.getPatientPhotos(patient: patientObject, isRadiography: isRadiography, completionFunction: { (patientPhotos:[PatientPhoto]?, err:Error?) in
            if err == nil, let photos = patientPhotos {
                self.patientPhotos = photos
            }
        })
    }
    
    func updateFinancialStatus(){
        self.dbService?.updateFinanceStatusOfPatient(patient: self.patientObject, financeObj: self.patientFinancialStatus, completionFunction: { (dbSuccess:Bool, dbError:Error?) in
            if dbSuccess {
                print("Finance object is changed successfully on db")
                self.fireService?.updateFinancialStatusOf(patient: self.patientObject, finance: self.patientFinancialStatus, completionFunction: { (success:Bool, error:Error?) in
                    if error == nil && success {
                        print("Finance object is changed successfully on firebase")
                    }
                    else {
                        self.showAlertController(withTitle: "Hata", andMessage: "Finansal durum güncellenemedi !")
                    }
                })
            }
            else {
                self.showAlertController(withTitle: "Hata", andMessage: "Finansal durum güncellenemedi !")
                print("\nWHILE UPDATING FINANCIAL STATUS AN ERROR OCCURED !!!!! ------> \(String(describing: dbError))")
            }
        })
    }
    
    func createFinancialHistory(){
        let newFinancialHistObj = FinancialHistory()
        newFinancialHistObj.patientID = self.patientObject.patientID
        newFinancialHistObj.paidAmount = self.patientFinancialStatus.paidAmount
        newFinancialHistObj.paymentDate = Date()
        
        self.fireService?.createFinancialHistoryForPayment(financialHist: newFinancialHistObj, completionFunction: { (success:Bool, error:Error?) in
            if success {
                print("\nFINANCIAL HISTORY OBJECT CREATED SUCCESSFULLY")
            }
            else {
                print("\nTHERE WAS AN ERROR WHILE CREATEING NEW FINANCIAL HISTORY OBJECT\n\(error!.localizedDescription)")
            }
        })
    }
    
    
    //MARK: Table View Related
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else {
            return 7
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentDetailCell", for: indexPath) as! AppointmentDetailTableViewCell
            cell.configureCellWith(financialStatus: self.patientFinancialStatus)
            financialNoteTextView = cell.financialNotes
            financialNoteTextView.delegate = self
            self.totalAmountTextfield = cell.totalAmountTextField
            self.paidAmountTextfield = cell.paidAmountTextField
            return cell
        }
        else if indexPath.section == 1 {
            let featureCell = tableView.dequeueReusableCell(withIdentifier: "PatientFeatureCell", for: indexPath)
            switch indexPath.row {
            case 0:
                featureCell.textLabel?.text = "Genel Diş Durumu"
            case 1:
                featureCell.textLabel?.text = "Hasta Filmleri"
            case 2:
                featureCell.textLabel?.text = "Hasta Fotoğrafları"
            case 3:
                featureCell.textLabel?.text = "Yeni Randevu"
            case 4:
                featureCell.textLabel?.text = "Randevu Geçmişi"
            case 5:
                featureCell.textLabel?.text = "Finansal Geçmiş"
            case 6:
                featureCell.textLabel?.text = "Hasta Bilgilerini Düzenle"
            default:
                return featureCell
            }
            
        }
        return tableView.dequeueReusableCell(withIdentifier: "PatientFeatureCell", for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                if let patient = patientObject {
                    self.showTeethGraph(ofPatient: patient)
                }
            case 1:
                self.isRadiography = true
                self.performSegue(withIdentifier: "showPatientPhotos", sender: self)
            case 2:
                self.isRadiography = false
                self.performSegue(withIdentifier: "showPatientPhotos", sender: self)
            case 3:
                self.performSegue(withIdentifier: "createAppointment", sender: self)
            case 4:
                self.performSegue(withIdentifier: "showAppointmentHistory", sender: self)
            case 5:
                self.performSegue(withIdentifier: "showFinancialHistory", sender: self)
            case 6:
                self.performSegue(withIdentifier: "editPatientInformations", sender: self)
            default:
                print("")
            }
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 242.0
        }
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "FINANS DETAYLARI"
        }
        return "HASTA DETAYLARI"
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    //MARK: - Delegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func noteChangeInAppointmentHist(currentAppointment: Appointment) {
        self.appointmentWithPatient.appointmentNotes = currentAppointment.appointmentNotes
        self.tableView.reloadData()
        self.delegate?.noteChanged()
    }
    
    //MARK: - Actions
    
    @IBAction func updateButtonAction(_ sender: UIButton) {
        let enteredTotalAmount = totalAmountTextfield.getIntegerValue
        let enteredPaidAmount = paidAmountTextfield.getIntegerValue
        let result = UtilFunctions.sharedInstance.shouldCreateFinancialHistObjectFor(finance: self.patientFinancialStatus, enteredTotalCost: enteredTotalAmount, enteredPaidAmount: enteredPaidAmount, enteredNotes: self.financialNoteTextView.text)
        
        if result.shoulCreateFinancialHist {
            self.createFinancialHistory()
        }
        self.patientFinancialStatus = result.finance
        updateFinancialStatus()
        tableView.reloadData()
    }
    
    func callPatient(){
        let phoneString = "tel://0\(appointmentWithPatient.patient!.patientPhone)"
        UIApplication.shared.openURL(URL(string: phoneString)!)
    }
    
    // MARK: - Navigation
    
    func showTeethGraph(ofPatient:Patient){
        let teetGraphVC = self.storyboard?.instantiateViewController(withIdentifier: "TeethGraph") as! TeethGraphVC
        teetGraphVC.selectedPatient = self.patientObject
        self.present(teetGraphVC, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPatientPhotos" {
            let destVC = segue.destination as! ImagePreviewCollectionVC
            destVC.isRadiography = self.isRadiography
            destVC.currentPatient = self.patientObject
        }
        else if segue.identifier == "showAppointmentHistory" {
            self.dbService?.getAppointmentHistoryOfPatient(patient: self.patientObject, completionFunction: { (returnedAppointments:[Appointment]?, error:Error?) in
                if error == nil && returnedAppointments != nil {
                    let destVC = segue.destination as! PatientAppointmentHistoryVC
                    destVC.appointmentHistory = returnedAppointments!
                    destVC.delegate = self
                    destVC.currentPatient = self.patientObject
                    destVC.navigationItem.title = "\(self.patientObject.patientName) \(self.patientObject.patientSurname)"
                }
                else {
                    print("\nTHERE WAS AN ERROR WHILE FETCHING APPOINTMENT HISTORY")
                }
            })
        }
        else if segue.identifier == "createAppointment" {
            let destVC = segue.destination as! CreateAppointmentVC
            destVC.selectedPatient = self.patientObject
        }
            
        else if segue.identifier == "showFinancialHistory" {
            self.dbService?.getPatientFinancialHistory(patient: self.patientObject, completionFunction: { (returnedHist:[FinancialHistory]?, error:Error?) in
                if error == nil {
                    let destVC = segue.destination as! FinancialHistoryVC
                    destVC.financialHistOfUser = returnedHist!
                }
                else {
                    print("\nTHERE WAS AN ERROR WHILE FETCHING FINANCIAL HISTORY")
                }
            })
        }
        else if segue.identifier == "editPatientInformations" {
            let destVC = segue.destination as! CreatePatientVC
            destVC.isCreatingProcess = false
            destVC.editingPatient = self.patientObject
        }
    }
}
