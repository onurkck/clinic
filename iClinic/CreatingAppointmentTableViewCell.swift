//
//  CreatingAppointmentTableViewCell.swift
//  iClinic
//
//  Created by Onur Küçük on 25.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class CreatingAppointmentTableViewCell: UITableViewCell {

    @IBOutlet weak var featureLabel: UILabel!
    @IBOutlet weak var featureDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWith(featureName:String, featureDesc:String?) {
        self.featureLabel.text = "\(featureName): "
        if featureDesc == nil {
            self.featureDescription.text = "Lütfen \(featureName) Seçiniz"
        }
        else{
            self.featureDescription.text = featureDesc!
        }
    }

}
