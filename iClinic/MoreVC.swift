//
//  MoreVC.swift
//  iClinic
//
//  Created by Onur Küçük on 9.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class MoreVC: BaseVC, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "iClinic"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table View Related Functions
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        if indexPath.section == 0 {
            cell.accessoryType = .none
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.text = "Randevu Oluştur" //TODO:Create appointment creator screen like teeth screen
        }
        else {
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Hastalarım"
            case 1:
                cell.textLabel?.text = "Klinik Bilgilerim"
            default:
                cell.textLabel?.text = ""
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            self.performSegue(withIdentifier: "CreateAppointment", sender: self)
        }
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                self.performSegue(withIdentifier: "showMyPatients", sender: self)
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
