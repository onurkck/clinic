//
//  PatientPhoto.swift
//  iClinic
//
//  Created by Onur Küçük on 26.12.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

class PatientPhoto: NSObject {
    var patientID = ""
    var isRadiography:Bool!
    var downloadURLString = ""
    
//    static func initPatientPhotoWith(parseObj:PFObject) -> PatientPhoto {
//        let patientPhoto = PatientPhoto()
//        patientPhoto.patientID = (parseObj.object(forKey: "patient") as! PFObject).objectId!
//        patientPhoto.imagePffile = parseObj.object(forKey: "photo") as! PFFile
//        patientPhoto.isRadiography = parseObj.object(forKey: "isRadiography") as! Bool
//        return patientPhoto
//    }
    static func initPatientPhotoWith(dictionary:NSDictionary) -> PatientPhoto {
        let patientPhoto = PatientPhoto()
        patientPhoto.patientID = dictionary.value(forKey: "patientID") as! String
        patientPhoto.downloadURLString = dictionary.value(forKey: "downloadURL") as! String
        return patientPhoto
    }

}
