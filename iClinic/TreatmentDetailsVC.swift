//
//  TreatmentDetailsVC.swift
//  iClinic
//
//  Created by Onur Küçük on 9.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit
protocol TreatmentSelectionDelegate {
    func treatmentSelected(selectedTreatment:Treatment)
}

class TreatmentDetailsVC: BaseVC, UITableViewDataSource, UITableViewDelegate {
    
    var treatmentTypes = [Treatment]()
    var selectedTreatment:Treatment? // or selected treatments
    var delegate:TreatmentSelectionDelegate?
    private var createdTreatmentName:Treatment?
    @IBOutlet weak var myTableView:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.createTreatment))
        getTreatments()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Backend Related 
    func getTreatments(){
//        dbService?.getTreatmentTypes(completionFunction: { (savedTreatments:[Treatment]?, error:Error?) in
//            if error == nil {
//                self.treatmentTypes = savedTreatments!
//                self.myTableView.reloadData()
//            }
//            else {
//                print(error!.localizedDescription)
//            }
//        })
    }
    
    func insertNewTreatment(){
//        dbService?.insertDatas(dbName: .Treatment, datas: [createdTreatmentName!.treatmentType], completionFunction: { (success:Bool, error:Error?) in
//            if error == nil && success == true {
//                print("TREATMENT Successfully Added ")
//                self.treatmentTypes.append(self.createdTreatmentName!)
//                self.myTableView.reloadData()
//            }
//            else {
//                self.showAlertController(withTitle: "Hata", andMessage: "Beklenmedik bir hata gerçekleşti lütfen tekrar deneyiniz.")
//                print("AN ERROR OCCURED WHILE INSERTING NEW TREATMENT \(error!.localizedDescription)")
//            }
//        })
    }
    
    //MARK: - Table View Related
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return treatmentTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TreatmentCell", for: indexPath)
//        if let treatment = selectedTreatment {
//            if treatment.treatmentType == treatmentTypes[indexPath.row].treatmentType {
//                cell.accessoryType = .checkmark
//            }
//        }
//        cell.textLabel?.text = treatmentTypes[indexPath.row].treatmentType
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.dequeueReusableCell(withIdentifier: "TreatmentCell", for: indexPath).accessoryType = .checkmark

//        self.delegate?.treatmentSelected(selectedTreatment: treatmentTypes[indexPath.row])
//        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Utils
    func createTreatment(){
        let alertController = UIAlertController(title: "Tedavi Ekle", message: "Lütfen eklemek istediğiniz tedavinin adını giriniz.", preferredStyle: .alert)
        alertController.addTextField { (tratmentTextField) in
            tratmentTextField.placeholder = "Tedavi Adı"
            tratmentTextField.textAlignment = .center
        }
        alertController.addAction(UIAlertAction(title: "Ekle", style: .default, handler: { (act) in
            if alertController.textFields?[0].text != "" {
//                self.createdTreatmentName = DBTreatment()
//                self.createdTreatmentName?.treatmentType = alertController.textFields?[0].text
                self.insertNewTreatment()
            }
            else {
                self.showAlertController(withTitle: "Hata", andMessage: "Bu alan boş bırakılamaz.")
            }
        }))
        alertController.addAction(UIAlertAction(title: "Vazgeç", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
