//
//  CreateAppointmentVC.swift
//  iClinic
//
//  Created by Onur Küçük on 24.11.2016.
//  Copyright © 2016 Onur Küçük. All rights reserved.
//

import UIKit

protocol AppointmentDelegate {
    func appointmentCreated()
}

class CreateAppointmentVC: BaseVC, UITableViewDataSource, UITableViewDelegate, PatientSelectionDelegate {
    @IBOutlet weak var myTableView:UITableView!    
    var patientsArray:[Patient]?
    var delegate:AppointmentDelegate?
    var startDate = Date()
    var selectedPatient:Patient?
    private var endDate = Date()
    private var startDateCellWillBeShown = false
    private var endDateCellWillBeShown = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        endDate = startDate.addingTimeInterval(3600) //Suppose that, an appointment takes an hour.
        self.navigationItem.title = "Yeni Randevu"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - DB Related
    
    func saveAppointment(){
        let newAppointmentObj = Appointment()
        newAppointmentObj.startDateTime = startDate as Date
        newAppointmentObj.endDateTime = endDate as Date
        newAppointmentObj.patient = self.selectedPatient!
        newAppointmentObj.patientID = self.selectedPatient!.patientID
        newAppointmentObj.appointmentNotes = "Randevu notlarınız."
        showActivityIndicator()
        let fireService:FirebaseService = FirebaseImplementation()
        fireService.createAppointment(appointment: newAppointmentObj) { (success:Bool, error:Error?) in
            self.stopActivityIndicator()
            if error == nil && success {
                self.delegate?.appointmentCreated()
                print("appointment created successfully")
                _ = self.navigationController?.popViewController(animated: true)
            }
            else {
                self.showAlertController(withTitle: "Hata", andMessage: "Randevu oluştururken beklenmedik bir hata oluştu lütfen tekrar deneyiniz.")
                print("CREATING APPOINTMENT ERROR IS -> \(error!.localizedDescription)")
            }
        }
    }

    //MARK: - Table View Related
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if startDateCellWillBeShown || endDateCellWillBeShown {
            return 5
        }
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CreatingAppointmentCell", for: indexPath) as! CreatingAppointmentTableViewCell
                cell.configureCellWith(featureName: "Hasta", featureDesc: selectedPatient?.patientName)
                return cell

            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CreatingAppointmentCell", for: indexPath) as! CreatingAppointmentTableViewCell
                cell.configureCellWith(featureName: "Başlangıç", featureDesc: DateConversion.sharedInstance.convertDateToCellFormat(date: startDate))
                return cell
            case 2:
                if startDateCellWillBeShown {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DatePickerCell", for: indexPath) as! DatePickerTableViewCell
                    cell.datePicker.tag = 1
                    cell.datePicker.date = startDate
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CreatingAppointmentCell", for: indexPath) as! CreatingAppointmentTableViewCell
                    cell.configureCellWith(featureName: "Bitiş", featureDesc: DateConversion.sharedInstance.convertDateToCellFormat(date: endDate))
                    return cell
                }
            case 3:
                if startDateCellWillBeShown {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CreatingAppointmentCell", for: indexPath) as! CreatingAppointmentTableViewCell
                    cell.configureCellWith(featureName: "Bitiş", featureDesc: DateConversion.sharedInstance.convertDateToCellFormat(date: endDate))
                    return cell
                }
                else if endDateCellWillBeShown {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DatePickerCell", for: indexPath) as! DatePickerTableViewCell
                    cell.datePicker.tag = 2
                    cell.datePicker.date = endDate
                    return cell
                }
                else {
                    return tableView.dequeueReusableCell(withIdentifier: "SaveAppointmentCell", for: indexPath)
                }
            case 4:
                return tableView.dequeueReusableCell(withIdentifier: "SaveAppointmentCell", for: indexPath)
            default:
                return tableView.dequeueReusableCell(withIdentifier: "SaveAppointmentCell", for: indexPath)
            }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        switch indexPath.row {
        case 0:
            startDateCellWillBeShown = false
            endDateCellWillBeShown = false
            tableView.reloadData()
            self.performSegue(withIdentifier: "showPatients", sender: self)
        case 1:
            startDateCellWillBeShown = true
            endDateCellWillBeShown = false
            tableView.reloadData()
        case 2:
            startDateCellWillBeShown = true
            endDateCellWillBeShown = false
            tableView.reloadData()
        case 3:
            if startDateCellWillBeShown || endDateCellWillBeShown {
                startDateCellWillBeShown = false
                endDateCellWillBeShown = true
            }
            else {
                self.createAppointment()
            }
            tableView.reloadData()
        case 4:
            self.createAppointment()
        default:
            print("Date selection cell touched")

        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if startDateCellWillBeShown {
            if indexPath.row == 2{
                return 222.0
            }
        }
        else if endDateCellWillBeShown {
            if indexPath.row == 3 {
                return 222.0
            }
        }
        return 44.0
    }
    
    //MARK: Helper
    func createAppointment(){
        if checkAllFieldsFilled() {
            saveAppointment()
        }
    }
    
    //MARK: - Delegate
    func patientSelected(selectedPatient: Patient) {
        self.selectedPatient = selectedPatient
        myTableView.reloadData()
    }
    
    @IBAction func dateChanged(_ sender: UIDatePicker) {
        if sender.tag == 1 {
            startDate = sender.date
            endDate = startDate.addingTimeInterval(3600)
        }
        else if sender.tag == 2 {
            endDate = sender.date
        }
        self.myTableView.reloadData()
    }
    
    //MARK: - Util
    func checkAllFieldsFilled() -> Bool{
        if selectedPatient != nil{
            return true
        }
        else {
            self.showAlertController(withTitle: "Hata", andMessage: "Lütfen tüm alanları doldurduğunuzdan emin olun.")
            return false
        }
    }
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPatients" {
            let destVC = segue.destination as! PatientVC
            destVC.delegate = self
            destVC.isCreatingAppointment = true
            if let patient = self.selectedPatient {
                destVC.selectedPatient = patient
            }
        }
    }

}
